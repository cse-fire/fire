import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.rst')).read()
CHANGES = open(os.path.join(here, 'CHANGES.txt')).read()

requires = [
    'pyramid>=1.5',
    'SQLAlchemy>=1.0.9',
    'transaction',
    'pyramid_tm',
    'pyramid_debugtoolbar',
    'pyramid_genshi>0.2',
    'pyramid_beaker',
    'pyramid_mailer',
    'pyramid_exclog',
    'zope.sqlalchemy',
    'waitress',
    'nose',
    'docopt',
    'deform<2',
    'apscheduler==3.0.6',
    'python-slugify',
    'mistune>=0.7.4',
    'asciimathml>=0.9.4',
    'colander<1.4'
    ]

setup(name='fire',
      version='2.2.0',
      description='fire',
      long_description=README + '\n\n' +  CHANGES,
      classifiers=[
        "Programming Language :: Python",
        "Framework :: Pylons",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
        ],
      author='',
      author_email='',
      url='',
      keywords='web wsgi bfg pylons pyramid',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,   # Useful to make the static dir available for direct serving
      test_suite='fire',
      install_requires=requires,
      entry_points="""\
      [paste.app_factory]
      main = fire:main
      [console_scripts]
      fireadmin = fire.scripts:fireadmin
      """,
      )

