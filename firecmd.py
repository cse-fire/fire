import sys
import os
import textwrap

from pyramid.paster import setup_logging, bootstrap, get_appsettings
from pyramid.request import Request

from fire.models import EmailVerification, CourseCode, User, Student, Settings
from fire import settings as fire_settings

import transaction

config_uri = os.getenv('FIRE_INI','fire.ini')

base_url = get_appsettings(config_uri).get('fire.url_host')
request = Request.blank('/', base_url=base_url)
env = bootstrap(config_uri, request=request)
setup_logging(config_uri)

start_transaction = transaction.manager.get
commit = transaction.manager.commit()


print("Beggin always by calling start_transaction(), and commit your changes with commit()")
