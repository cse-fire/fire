import pytest

from fire.models import CheckHook


@pytest.fixture()
def autocheck():
    return CheckHook(
        url_template="https://jenkins.ci/run?submission=$submission_url&token=$feedback_url")


def test_get_url(autocheck):
    """ Test url instanciation """
    expected = "https://jenkins.ci/run?submission=abc&token=def"
    actual = autocheck.get_url('abc', "def")
    assert actual == expected


def test_get_url_escape(autocheck):
    """ Test that instanciated url variables are properly quoted """
    expected = "https://jenkins.ci/run?submission=a/b%20c&token=d%3Fe%26f"
    actual = autocheck.get_url('a/b c', "d?e&f")
    assert actual == expected
