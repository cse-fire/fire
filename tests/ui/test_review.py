import time

from utils import login


def test_shared_review(server, browser, submission, grader, othergrader):
    """ Test that review draft are shared between graders """
    report_url = server.mkurl("admin/labs/%d/submissions/%s" %
                              (submission.lab.id, submission.submitter.slug))
    login(server, browser, grader)
    browser.get(report_url)
    browser.find_element_by_name("review").send_keys("REVIEW 1234")
    browser.find_element_by_name("grade").send_keys("F")
    browser.find_element_by_name("note").send_keys("NOTE 4321")
    time.sleep(5)  # give time for auto save
    browser.get(server.mkurl('logout'))

    login(server, browser, othergrader)
    browser.get(report_url)
    browser.find_element_by_link_text("Show the review form anyway.").click()
    assert (browser.find_element_by_name('review').get_attribute("value") ==
            "REVIEW 1234")
    assert (browser.find_element_by_name('grade').get_attribute("value") ==
            "F")
    assert (browser.find_element_by_name('note').get_attribute("value") ==
            "NOTE 4321")
