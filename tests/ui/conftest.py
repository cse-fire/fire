import logging
import os
from threading import Thread
from time import sleep
from urlparse import urljoin

import pytest
import transaction
from wsgiref.simple_server import make_server
from selenium.webdriver import Firefox, PhantomJS

import fire
from fire.models import (
    DBSession, Base, CourseCode,
    Student, Grader, Group, Submission,
    Lab, GroupLab, GraderWorkload, Markdown)

PASSWORD = "p4ssw0rd"


hdr = logging.StreamHandler()
hdr.setLevel(logging.WARN)
formatter = logging.Formatter('%(name)s - %(levelname)s - %(message)s')
hdr.setFormatter(formatter)
logging.root.addHandler(hdr)
logging.getLogger('fire').setLevel(logging.DEBUG)


class TestServer(object):
    """ Starts a fire server for the selenium tests """

    def __init__(self, datadir, port=4242):
        self.datadir = datadir
        self.port = port
        self.server = make_server('', port, fire.main({}, **self.settings()))

    def settings(self):
        return {
            'fire.debug': 'true',
            'fire.auth_tkt_secret': 'test',
            'fire.course_slug': 'test',
            'fire.data_dir': self.datadir,
            'mail.queue_path': os.path.join(self.datadir, 'mailq'),
            'mail.default_sender': 'noreply@fire.cse.chalmers.se',
            'fire.url_host': "localhost",
        }

    def start(self):
        print "Starting server... (data: %s)" % self.datadir
        Thread(target=self.server.serve_forever).start()
        sleep(2)

    def stop(self):
        self.server.shutdown()

    def mkurl(self, path):
        """Method to create an absolute url for the test server
        >>> testurl("login")
        "http://localhost:8521/test/login"
        """
        return urljoin("http://localhost:%d/" % (self.port), path)


@pytest.fixture(scope="session", autouse=True)
def server(request, tmpdir_factory):
    server = TestServer(str(tmpdir_factory.mktemp('data')))
    server.start()
    request.addfinalizer(server.stop)
    return server


@pytest.fixture(scope="function", autouse=True)
def fresh_database(request, server):
    # Create the database
    Base.metadata.create_all(DBSession.bind)

    # Close the session and clear the database after each test
    def fin():
        Base.metadata.drop_all(DBSession.bind)
        DBSession.remove()
    request.addfinalizer(fin)
    return DBSession()


@pytest.fixture
def course_code(fresh_database):
    code = "XXX666"
    with transaction.manager:
        DBSession.add(CourseCode(code=code))
        DBSession.flush()
    return code


@pytest.fixture
def student(fresh_database):
    student = Student(
        "Bart Simpson <bsimpson@sudents.springfield.edu>", password=PASSWORD)
    student.group = Group(id=1)
    with transaction.manager:
        DBSession.add(student)
        DBSession.flush()
    return student


@pytest.fixture
def grader(fresh_database):
    grader = Grader(
        "Edna Krabappel <krabappel@springfield.edu>", password=PASSWORD)
    with transaction.manager:
        DBSession.add(grader)
        DBSession.flush()
    return grader


@pytest.fixture
def othergrader(fresh_database):
    grader = Grader(
        "Seymour Skinner <skinner@springfield.edu>", password=PASSWORD)
    with transaction.manager:
        DBSession.add(grader)
        DBSession.flush()
    return grader


@pytest.fixture(params=['individual', 'group'])
def lab(request, fresh_database, grader):
    if request.param is 'individual':
        lab = Lab(title="My lab")
    else:
        lab = GroupLab(title="My lab")
    lab.workload = [GraderWorkload(lab, grader, 1)]
    with transaction.manager:
        DBSession.add(lab)
        DBSession.flush()
    return lab


@pytest.fixture
def submission(fresh_database, lab, grader, student):
    with transaction.manager:
        lab = DBSession.merge(lab)
        DBSession.refresh(lab)
        student = DBSession.merge(student)
        DBSession.refresh(student)
        if lab.individual:
            submission = Submission(lab, student, Markdown("Foobar"), False)
        else:
            submission = Submission(lab, student.group, Markdown("Foobar"), False)
        submission.assign_grader(grader)
        fresh_database.add(submission)
        fresh_database.flush()
        return submission


@pytest.fixture
def rejectedsubmission(fresh_database, lab, grader, student):
    with transaction.manager:
        lab = DBSession.merge(lab)
        DBSession.refresh(lab)
        student = DBSession.merge(student)
        DBSession.refresh(student)
        if lab.individual:
            submission = Submission(lab, student, Markdown("Foobar"), False)
        else:
            submission = Submission(lab, student.group, Markdown("Foobar"), False)
        submission.assign_grader(grader)
        submission.post_review(False, grader, None, None, Markdown('Baaaaaaaaad'))
        fresh_database.add(submission)
        fresh_database.flush()
        return submission


@pytest.fixture(scope="function")
def browser(request):
    # Setup selenium driver
    b = PhantomJS()
    b.set_window_size(1920, 1920)
    b.implicitly_wait(20)
    # close the browser after the test
    print request
    print request.node.name

    def screenshot_and_close():
        b.get_screenshot_as_file(request.node.name + '.png')
        b.close()

    request.addfinalizer(screenshot_and_close)
    return b
