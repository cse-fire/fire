from utils import login, fill


def test_create_grouplab(server, browser, grader):
    TITLE = "Rocket building"
    SLUG = "Rocket"
    DEADLINE1 = "2042-12-24 23:59"
    DEADLINE2 = "2042-12-31 23:59"

    login(server, browser, grader)

    browser.find_element_by_id('new-group-lab-button').click()
    browser.find_element_by_name("title").send_keys(TITLE)
    browser.find_element_by_name("slug").send_keys(SLUG)
    browser.find_element_by_name("first_deadline").send_keys(DEADLINE1)
    browser.find_element_by_name("final_deadline").send_keys(DEADLINE2)
    browser.find_element_by_name("allowed_file_extensions").send_keys("pdf")
    browser.find_element_by_name("min_members").send_keys("2")
    browser.find_element_by_name("max_members").send_keys("3")
    browser.find_element_by_name("Add").click()

    browser.find_element_by_partial_link_text(TITLE).click()
    browser.get_screenshot_as_file('test_create_grouplab.png')



def test_create_individual_lab(server, browser, grader):
    TITLE = "Rocket building"
    SLUG = "Rocket"
    DEADLINE1 = "2020-02-29 00:00"
    DEADLINE2 = "2020-03-18 00:00"

    login(server, browser, grader)

    browser.find_element_by_id('new-individual-lab-button').click()
    browser.find_element_by_name("title").send_keys(TITLE)
    browser.find_element_by_name("slug").send_keys(SLUG)
    browser.find_element_by_name("first_deadline").send_keys(DEADLINE1)
    browser.find_element_by_name("final_deadline").send_keys(DEADLINE2)
    browser.find_element_by_name("allowed_file_extensions").send_keys("pdf")
    browser.find_element_by_name("Add").click()

    browser.find_element_by_partial_link_text(TITLE).click()

    browser.get_screenshot_as_file('test_create_individual_lab.png')

    
def test_create_lab_past_deadline(server, browser, grader):
    DEADLINE1 = "1971-01-01 00:05"
    DEADLINE2 = "1969-12-31 23:59"

    login(server, browser, grader)

    browser.find_element_by_id('new-individual-lab-button').click()
    

    fill(browser,"first_deadline",DEADLINE1)
    assert "The first deadline has passed." in [
        a.text for a in browser.find_elements_by_class_name("help-inline")]

    fill(browser,"final_deadline",DEADLINE2)
    assert "The final deadline is set before the first." in [
        a.text for a in browser.find_elements_by_class_name("help-inline")]

    browser.get_screenshot_as_file('test_create_lab_past_deadline.png')

 
