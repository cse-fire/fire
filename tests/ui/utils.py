from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By

def login(server, browser, user, password='p4ssw0rd'):
    """ Log-in with the given user.  The password needs to be given separately
    because the User object will not give the password back. """
    browser.get(server.mkurl("login"))
    browser.find_element_by_name("email").send_keys(user.email)
    browser.find_element_by_name("password").send_keys(password)
    browser.find_element_by_id('loginbtn').click()

def fill(browser, name, text):
    """ Fills a field and makes it lose focus """
    browser.find_element_by_name(name).send_keys(text)
    browser.find_element_by_tag_name("html").click()

def click_when_able(browser, obj_id):
    wait = WebDriverWait(browser, 10)
    element = wait.until(EC.element_to_be_clickable((By.ID, obj_id)))
    element.click()

