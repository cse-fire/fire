from utils import login, fill

def test_update_lab_past_deadline(server, browser, grader, lab):
    DEADLINE1 = "1971-01-01 00:05"
    DEADLINE2 = "1969-12-31 23:59"

    login(server, browser, grader)

    browser.find_element_by_partial_link_text(lab.title).click()
    browser.find_element_by_id("lab-settings-button").click()

    fill(browser,"first_deadline",DEADLINE1)
    assert "The first deadline has passed." in [
        a.text for a in browser.find_elements_by_class_name("help-inline")]

    fill(browser,"final_deadline",DEADLINE2)
    assert "The final deadline is set before the first." in [
        a.text for a in browser.find_elements_by_class_name("help-inline")]

    browser.get_screenshot_as_file('test_update_lab_past_deadline.png')
 
