from selenium.webdriver.support.wait import WebDriverWait
import selenium.webdriver.support.expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

from fire.models import DBSession, EmailVerification


def test_registration(server, browser, course_code):
        browser.get(server.mkurl("login"))

        browser.find_element_by_id("signup-btn").click()
        field = browser.find_element_by_name("signup_email")
        field.send_keys("test@test.com")
        field.send_keys(Keys.RETURN)

        msg = browser.find_element_by_class_name('alert-success').text
        assert msg == "A verification link has been sent to test@test.com."

        code = DBSession.query(EmailVerification)\
            .filter(EmailVerification.email == 'test@test.com')\
            .all()[0].token
        browser.get(server.mkurl("login/new?token=%s" % code))

        browser.find_element_by_name("first_name").send_keys("Lisa")
        browser.find_element_by_name("last_name").send_keys("Simpson")
        browser.find_element_by_name("id_number").send_keys("3011195876")
        browser.find_element_by_name("password").send_keys("0"*12)
        browser.find_element_by_name("password-confirm").send_keys("0"*12)

        browser.find_element_by_id('signupformregister').click()

        browser.find_element_by_name("email").send_keys("test@test.com")
        browser.find_element_by_name("password").send_keys("0"*12)
        browser.find_element_by_id('loginbtn').click()

        profile_btn = browser.find_element_by_id("profile-btn")
        assert profile_btn.text == "Lisa Simpson"

        profile_btn.click()


