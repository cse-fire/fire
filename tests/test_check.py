import pytest

from fire.models import Lab, AllowedFileExtension

from fire.check import check_extension


@pytest.fixture
def filename():
    """ Return a filename """
    return "foo.bar.baz"


@pytest.fixture
def lab():
    """ Return a lab """
    return Lab(title="Test Lab", slug="lab")


class TestCheckExtension:

    def test_empty_allowed(self, filename, lab):
        assert check_extension(filename, lab)

    def test_included_allowed(self, filename, lab):
        lab.allowed_file_extensions += [AllowedFileExtension(extension=".baz")]
        assert check_extension(filename, lab)

    def test_other_forbidden(self, filename, lab):
        lab.allowed_file_extensions += [AllowedFileExtension(extension=".bar")]
        assert not check_extension(filename, lab)
