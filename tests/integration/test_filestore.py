import os
from StringIO import StringIO

import pytest

from fire.filestore import DraftDir
from fire.models import Lab, Student, Group


class TestDraftDir(object):

    def test_path_student(self, tmpdir):
        lab = Lab(id=1, title="Foobar")
        student = Student("test@test.com")
        draftdir = DraftDir(str(tmpdir), lab, student)
        assert (
            draftdir.path ==
            os.path.join(str(tmpdir), 'drafts/lab01/test@test.com'))

    def test_path_group(self, tmpdir):
        lab = Lab(id=1, title="Foobar")
        group = Group(id=3)
        draftdir = DraftDir(str(tmpdir), lab, group)
        assert (
            draftdir.path ==
            os.path.join(str(tmpdir), 'drafts/lab01/group03'))

    @pytest.fixture
    def draftdir(self, tmpdir):
        lab = Lab(id=1, title="Foobar")
        student = Student("test@test.com")
        return DraftDir(str(tmpdir), lab, student)

    def test_store_file(self, draftdir):
        draftdir.store_file('foobar.txt', StringIO("foobardata"))
        assert len(draftdir.ls()['files']) == 1
        file = draftdir.ls()['files'][0]
        assert file.name == 'foobar.txt'
        filecontent = open(file.path).read()
        assert filecontent == 'foobardata'

    def test_delete_file(self, draftdir):
        draftdir.store_file('foobar.txt', StringIO("foobardata"))
        draftdir.delete_file('foobar.txt')
        assert len(draftdir.ls()['files']) == 0
