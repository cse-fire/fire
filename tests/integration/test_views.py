import mock
import pytest

from fire.views.api import check_feedback
from fire.views.admin import AdminGradersView
from fire.views.signup import SignupView


class TestCheckFeeback(object):
    """ Test the view check_feedback that accepts feedback from automated
    checks """

    @pytest.fixture
    def request(self):
        req = mock.Mock()
        req.matchdict = {'token': 'xxxxxxxxxxxx'}
        req.params = {'status': 'pass'}
        return req

    def test_pass(self, request):
        """ Test setting status PASS on a pending job """
        with mock.patch('fire.views.api.CheckResult') as CheckResult:
            CheckResult.get.return_value = CheckResult
            job = CheckResult.get()
            job.status = CheckResult.PENDING
            response = check_feedback(request)
            assert job.status is CheckResult.PASS
            print "response", response.status
            assert response.status == '204 No Content'

    def test_fail(self, request):
        """ Test setting status FAIL on a pending job """
        request.params['status'] = 'fail'
        with mock.patch('fire.views.api.CheckResult') as CheckResult:
            CheckResult.get.return_value = CheckResult
            job = CheckResult.get()
            job.status = CheckResult.PENDING
            response = check_feedback(request)
            assert job.status is CheckResult.FAIL
            assert response.status == '204 No Content'

    def test_invalid_token(self, request):
        """ Test setting status PASS on a non-existing job """
        request.params['status'] = 'fail'
        with mock.patch('fire.views.api.CheckResult') as CheckResult:
            CheckResult.get.return_value = None
            response = check_feedback(request)
            assert response.status == '404 Not Found'

    def test_used_token(self, request):
        """ Test setting status PASS on a failed job """
        with mock.patch('fire.views.api.CheckResult') as CheckResult:
            CheckResult.get.return_value = CheckResult
            job = CheckResult.get()
            job.status = CheckResult.FAIL
            response = check_feedback(request)
            assert job.status is CheckResult.FAIL
            assert response.status == '410 Gone'

    def test_add_details_url(self, request):
        request.body = 'http://example.com'
        with mock.patch('fire.views.api.CheckResult') as CheckResult:
            CheckResult.get.return_value = CheckResult
            job = CheckResult.get()
            job.status = CheckResult.PENDING
            assert check_feedback(request).code == 204 # HTTPNoContent
            assert job.details_url.source() == 'http://example.com'
