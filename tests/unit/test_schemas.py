from datetime import datetime

import colander
import deform
import pytest
from unittest import TestCase

from fire.models import Lab, AllowedFileExtension
from fire.views.settings import IndividualLabSchema

class TestRegisterSchema(TestCase):
    """ Test the schema used for registering. Only require an email. """

    def setUp(self):
        from fire.schemata import RegisterSchema
        self.schema = RegisterSchema()

    def test_valid(self):
        """ Test deserialization of valid data """
        data = dict(signup_email='root@the-machine.us')
        self.assertEquals({'signup_email': 'root@the-machine.us'},
                          self.schema.deserialize(data))

    def test_invalid(self):
        """ A non correct email address should be refused """
        data = dict(signup_email='grump@. ')
        with self.assertRaises(colander.Invalid):
            self.schema.deserialize(data)

    def test_plus(self):
        """ An adress with a + in the user part should be accepted """
        self.schema.deserialize({'signup_email': 'name+filter@test.com'})

    def test_upper(self):
        """ Upper case letters shuld be converted to lower case """
        self.assertEquals({'signup_email': 'test@test.com'},
                          self.schema.deserialize(dict(signup_email='TeST@teST.Com')))


class TestCourseCodeNode(TestCase):
    """ Test the node for course codes """

    def setUp(self):
        from fire.schemata import CourseCodeNode
        course_codes = ['DAT101', 'DIT101']
        self.node = CourseCodeNode().bind(course_codes=course_codes)

    def test_valid(self):
        """ Deserialization of valid data """
        self.assertEquals('DAT101', self.node.deserialize('DAT101'))

    def test_widget(self):
        """ Test that the node uses a select widget.
        It would be nice to do:
        self.assertEquals(
                deform.widget.SelectWidget(
                      values=[('DAT101','DAT101'),('DIT101','DIT101')]),
                self.node.widget )
        but apparently deform widgets don't support the comparison so this
        needed to be split in two assertions
        """
        self.assertIsInstance(self.node.widget, deform.widget.SelectWidget)
        self.assertEquals(self.node.widget.values,
                          [('DAT101', 'DAT101'), ('DIT101', 'DIT101')])

    def test_invalid(self):
        """ Given a course code not present in the bound values, should
        raise an invalid exception """
        with self.assertRaises(colander.Invalid):
            self.node.deserialize('AAA000')


class TestPersonNummerNode(TestCase):
    """ Test the preparer function for the PersonNummer schema node """

    def setUp(self):
        from fire.schemata import PersonNummerNode
        self.node = PersonNummerNode()

    def test_valid(self):
        """ Test that a valid pn is deserialized as is """
        self.assertEquals('9212214456', self.node.deserialize('9212214456'))

    def test_dash(self):
        """ Test that dashes are ignored """
        self.assertEquals('9212214456', self.node.deserialize('921221-4456'))

    def test_century(self):
        """ Test that the century is ignored """
        self.assertEquals('9212214456', self.node.deserialize('199212214456'))

    def test_invaldi(self):
        """ Test that an invalid pn is refused """
        with self.assertRaises(colander.Invalid):
            self.node.deserialize('zzz')


class TestStudentSettingsSchema(TestCase):
    """ Test the schema used for entering/changing student info """
    pass

    def setUp(self):
        from fire.schemata import StudentSettingsSchema
        self.schema = StudentSettingsSchema().bind(course_codes=['ANO101', 'ABC123'])
        self.data = dict(
            first_name='John',
            last_name='Reese',
            id_number='0000000000',
            course="ANO101"
        )

    def test_valid(self):
        """ Test deserialization of valid data """
        self.assertEquals(self.data, self.schema.deserialize(self.data))

    def test_missing_first_name(self):
        """ if the first name is missing, the data is invalid """
        with self.assertRaises(colander.Invalid):
            del self.data['first_name']
            self.schema.deserialize(self.data)

    def test_empty_first_name(self):
        """ if the first name is '', the data is invalid """
        with self.assertRaises(colander.Invalid):
            self.data['first_name'] = ''
            self.schema.deserialize(self.data)

    def test_missing_last_name(self):
        """ if the last name is missing, the data is invalid """
        with self.assertRaises(colander.Invalid):
            del self.data['last_name']
            self.schema.deserialize(self.data)

    def test_empty_last_name(self):
        """ if the last name is '', the data is invalid """
        with self.assertRaises(colander.Invalid):
            self.data['last_name'] = ''
            self.schema.deserialize(self.data)

    def test_missing_pn(self):
        """ Missing personnummer """
        with self.assertRaises(colander.Invalid):
            del self.data['id_number']
            self.schema.deserialize(self.data)

    def test_empty_pn(self):
        """ Empty personnummer """
        with self.assertRaises(colander.Invalid):
            self.data['id_number'] = ''
            self.schema.deserialize(self.data)

    def test_invalid_pn(self):
        """ Empty personnummer """
        with self.assertRaises(colander.Invalid):
            self.data['id_number'] = 'XXX'
            self.schema.deserialize(self.data)

    def test_missing_course(self):
        """ Missing course code """
        with self.assertRaises(colander.Invalid):
            del self.data['course']
            self.schema.deserialize(self.data)

    def test_invalid_course(self):
        """ Course code which is not in the accepted list """
        with self.assertRaises(colander.Invalid):
            self.data['course'] = "XXX000"
            print self.schema.deserialize(self.data)


class TestStudentCreateAccountSchema(TestCase):
    """ Test the schema used by students when creating an account """

    def setUp(self):
        from fire.schemata import StudentCreateAccountSchema
        self.schema = StudentCreateAccountSchema().bind(course_codes=['ANO101', 'ABC123'])
        self.data = dict(
            first_name=u'John', last_name=u'Reese',
            id_number=u'0101010101', course=u'ANO101',
            password=u'correct horse battery staple',
        )

    def test_valid_data(self):
        """ Passing valid data to deserialize should return the data unchanged """
        self.assertEquals(self.schema.deserialize(self.data), self.data)

    def test_password_too_short(self):
        """ A password of less than 6 characters is too short """
        self.data['password'] = self.data['confirm_password'] = 'baddd'
        with self.assertRaises(colander.Invalid):
            self.schema.deserialize(self.data)


class TestLabSchema:
    """ Test the schema used for creating and modifying labs. """

    @pytest.fixture
    def cstruct(self):
        """ The form data """
        return dict(
            title="Foobar lab",
            slug="Foobar",
            autoassign_grader=False,
            predecessor='1',
            first_deadline="2042-11-12T23:59",
            final_deadline="2042-12-24T23:59",
            allowed_file_extensions=['hs', ' .txt'],
            allow_physical_submissions='true')


    @pytest.fixture
    def appstruct(self):
        """ The application data structure """
        return dict(
            title="Foobar lab",
            slug="Foobar",
            autoassign_grader=False,
            predecessor=1,
            first_deadline=datetime(2042, 11, 12, 23, 59),
            final_deadline=datetime(2042, 12, 24, 23, 59),
            allowed_file_extensions=[
                AllowedFileExtension(extension='.hs'),
                AllowedFileExtension(extension='.txt')
            ],
            allow_physical_submissions=True)


    @pytest.fixture
    def schema(self):
        """ Schema under test """
        return IndividualLabSchema().bind(labs=[Lab("foobar", id=1)])

    def test_deserialize_valid_data(self, schema, cstruct, appstruct):
        """ Test deserialization of valid data """
        assert schema.deserialize(cstruct) == appstruct

    def test_deserialize_data_with_no_predecessor(self, schema, cstruct, appstruct):
        del cstruct['predecessor']
        appstruct['predecessor'] = None
        assert schema.deserialize(cstruct) == appstruct
