var $ = require('jquery');

function rich_datetimepicker (picker) {
  var field = picker.find(".input-append, .date")
  var input = field.find("input")
  var msg = picker.find(".help-inline")

  function set_warning (warning_msg) {
    picker.addClass("warning");
    msg.text(warning_msg)
  }

  function reset_msg () {
    picker.removeClass("warning")
    msg.text("")
  }

  function change (f) {
    field.on('changeDate', function(e) { return f(); });
  }

  function val_date () {
    return field.data("datetimepicker").getDate();
  }

  return { change: change
         , input: input
         , val_date: val_date
         , set_warning: set_warning
         , reset_msg: reset_msg
         }
}

var Fire = {

  setup_deadline_picker : function(first, last) {

    var ofirst = rich_datetimepicker(first);
    var olast = rich_datetimepicker(last);

    function validate () {

      var date_now = (new Date());
      var date_first = ofirst.val_date();
      var date_final = olast.val_date();

      ofirst.reset_msg();
      if (ofirst.input.val() && date_first < date_now) {
        ofirst.set_warning("The first deadline has passed.");
      }

      olast.reset_msg();
      if (olast.input.val()) {
        if (date_final < date_first) {
          olast.set_warning("The final deadline is set before the first.");
        } else if (date_final < date_now) {
          olast.set_warning("The final deadline has passed.");
        }
        /* In case that the final deadline is both in the past, and before the
           first, we only warn that the final deadline comes before the first.

           Having both warnings is unnecessarily verbose, unless there is 
           a situation where the user would want to have a final deadline that
           is both i) in the future (i.e. not in the past), and 
           ii) before the first deadline.
        */
      }
    }

    ofirst.change(validate)
    olast.change(validate)

    return { validate : validate }
  }
}

return Fire;
