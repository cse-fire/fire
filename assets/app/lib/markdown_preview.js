var $ = require('jquery');

var Markdown = {
  /**
   * Markdown field with preview for bootstrap.
   *
   * opts is a record with three fields:
   *
   * tab: bootstrap tab that should show the preview.
   * well: div that will contain the rendered text.
   * field: textarea where the Markdown is written to.
   */
  setup_markdown_previewer: function(opts) {
    var source = opts.source;
    var preview = opts.preview;
    var toggle = opts.toggle;
    preview.hide();
    toggle.bind('click', function() {
      if (preview.is(':visible')) {
        preview.hide();
        source.show();
      } else {
        $.post(source.data('markdown-render-url'),
          $.param({source: source.val()}))
          .done(function (data) {
            preview.html(data.content);
            preview.show();
            source.hide();
          }).fail(function () {
            alert('Preview unavailable')
        });
      }
      return true;
    });
  }
};

return Markdown;
