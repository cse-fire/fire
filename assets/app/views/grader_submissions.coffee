define ["jquery", "lib/markdown_preview"], ($) -> $ ->
  Markdown = require("lib/markdown_preview")

  $('[data-toggle=tooltip]').tooltip()

  $('.fileaction').each ->
    $this = $(this)
    subid = $this.closest('.submission').data('submission')
    filename = $this.closest('tr').data('filename')
    file_url = '../../../../download/' + subid + '/' + encodeURIComponent(filename)
    switch $this.data('action')
      when "download"
        $this.click () ->
          window.location.href = file_url + "?dl=1"
      when "browse"
        $this.attr('href', file_url)

  $('#reassign-grader select').change ->
    $('#reassign-grader').submit()

  $('.check-output-show-full').click ->
    $(this).parent().removeClass('truncated')
    $(this).hide()

  $('.markdown').each (i, m) ->
    Markdown.setup_markdown_previewer {
      source:  $(m).find('.markdown-source'),
      preview: $(m).find('.markdown-preview'),
      toggle:  $(m).find('.markdown-toggle')
    }

  $('.submission-review-form').each (i, f) ->
    form = $(f)
    statusbar = $(f).find('.submission-status')
    if not form.length
      return

    autosave_interval = 3000
    autosave_lastval = form.serialize()
    autosave_time = -1
    autosave_ongoing = false

    autosave = ->
      newdata = form.serialize()
      if !autosave_ongoing and newdata != autosave_lastval
        now = new Date()
        if now.getTime() - autosave_time > autosave_interval
          $.post form.data('autosave-url'), newdata, (data) ->
            if data.error
              statusbar.text('Saving failed: ' + data.error)
            else
              autosave_lastval = newdata
              autosave_time = now.getTime()
              statusbar.text('Saved at ' + now.toLocaleTimeString())
      setTimeout autosave, autosave_interval

    autosave()

  $('.submission .minimizer').click () ->
    submission = $(this).parent()
    submission.addClass "submission-minimized"
    return false

  $('.submission').click () ->
    if $(this).hasClass "submission-minimized"
      $(this).removeClass "submission-minimized"