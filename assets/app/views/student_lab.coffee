define ["jquery", "jquery.fileupload", "lib/markdown_preview"], ($) ->
  $ ->
    $('[data-toggle=tooltip]').tooltip()

    Markdown = require("lib/markdown_preview")

    activeUploads = 0

    # A global variable to track new uploads to the submission
    newfiles = false

    $("#fileupload").fileupload
      dataType: "json"
      add: (e, data) ->
        ++activeUploads
        $('#submit-button').button('loading')
        data.submit()
      submit: (e, data) ->
        tr = $('<tr/>')
        td = $('<td class="filename"/>')
        console.log(data.files)
        td.text(data.files[0].name)
        tr.append(td)
        tr.append("<td/>")
        tr.append('<td colspan="2" class="message"><div class="progress"><div class="bar" style="width: 0%;"></div></div></td>')
        data.tableRow = tr
        $('#filelisting tbody').append(tr)
      done: (e, data) ->
        if data.result.error
          $('.message', data.tableRow).addClass('message-error').text(data.result.error)
        else
          newrow = $(data.result.tablerow)
          setupFileActions(newrow)
          $(data.tableRow).replaceWith(newrow)
          if --activeUploads == 0
            $('#submit-button').button('reset')
          newfiles = true
      fail: (e, data) ->
        $('.message', data.tableRow).addClass('message-error').text("Upload failed: " + data.jqXHR.statusText)
      progress: (e, data) ->
        progress = parseInt(data.loaded / data.total * 100, 10);
        bar = $(".progress .bar", data.tableRow).css('width', progress + '%')
        if progress >= 95
          bar.addClass("progress-striped active") # things hang a bit at 100%, show that its not frozen

    $('#browse-button').click () ->
      $('#fileupload').click()

    $('.modal-cancel').click () ->
      $(this).closest('.modal').modal('hide')

    setupFileActions = (tr) ->
      $tr = $(tr)
      $('.fileaction', tr).click () ->
        action = $(this).data('action')
        filename = $tr.data('filename')
        switch action
          when "delete"
            mdl = $('#modal-confirm-delete')
            mdl.find('#confirm-delete-filename').text(filename)
            mdl.find('#confirm-delete-button').unbind('click').click () ->
              mdl.modal('hide')
              url = $(this).data('url')
              $.post url, filename, (data) ->
                if data.error
                  alert(data.error)
                else
                  tbody = $tr.parent()
                  $tr.slideUp -> $tr.remove()
            mdl.modal()
            break
          when 'download'
            subid = $tr.closest('.submission').data('submission')
            window.location.href = '../download/' + subid + '/' + encodeURIComponent(filename) + '?dl=1';
          when 'download-draft'
            window.location.href += '/draft/files/' + encodeURIComponent(filename) + '?dl=1';

    $('.filelisting-5col tr').each (i, tr) ->
      setupFileActions(tr)

    $('.check-output-show-full').click ->
      $(this).parent().removeClass('truncated')
      $(this).hide()

    $('.markdown').each (i, m) ->
      Markdown.setup_markdown_previewer {
        source:  $(m).find('.markdown-source'),
        preview: $(m).find('.markdown-preview'),
        toggle:  $(m).find('.markdown-toggle')
      }

    $('#submit-button').click () ->
      mdl = $('#modal-confirm-submit')
      # TODO add extra info to modal, e.g. if there are no files etc.
      $('input[name=comment]').val($('#comment_edit_field').val())
      if $('#grader')
        $('input[name=grader]').val($('#grader').val())
      p = $('#physical_submission')
      if p.length and p.is(':checked')
        $('input[name=physical]').val('yes')
      mdl.modal()

    # If new files have been uploaded but then the student leaves the page, we
    # show a confirmation dialog reminding them that the files are not
    # submitted
    $(window).bind('beforeunload', () ->
      if newfiles
        return 'Uploaded files are not considered submitted until you press the submit button.'
    )

    # When the student confirms the submission, we need to set newfiles to
    # false so that the confirmation alert is not show
    $('#confirm-submit-button').click () ->
      newfiles = false
      return true

    $('.submission .minimizer').click () ->
      submission = $(this).parent()
      submission.addClass "submission-minimized"
      return false

    $('.submission').click () ->
      if $(this).hasClass "submission-minimized"
        $(this).removeClass "submission-minimized"