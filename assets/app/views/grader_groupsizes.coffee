$ ->
  $("#groupsizes form").each ->
    deform.addCallback(
      $(this)[0].id,
      (oid) ->
        target = '#' + oid
        $(target).ajaxForm {
          target: target,
          dataType: 'json',
          beforeSubmit: ->
            $('body').css('cursor', 'wait')
          ,
          success: (response) ->
            td = $(target).parent()
            if response.added
              td.removeClass('wrong-size')
            else
              if td.data('wrongsize') == 1
                td.addClass('wrong-size')

            $('body').css('cursor', 'default')
          ,
          error: () ->
            alert("FAIL")
        }
    )

  $('#groupsizes input').change ->
    $('#' + this.id + '_form').submit()