define ["jquery", "lib/fire"], ($) ->
  $ ->
    Fire = require("lib/fire");

    $(".deadline-extension").each ->
      first_oid = $(this).data('first-deadline-oid')                                      
      final_oid = $(this).data('final-deadline-oid')

      Fire.setup_deadline_picker($('#' + first_oid), $('#' + final_oid))