define ["jquery", "lib/fire"], ($) ->
  $ ->
    Fire = require("lib/fire");

    deadline_first = $('input[name="first_deadline"]').parents(".control-group")
    deadline_final = $('input[name="final_deadline"]').parents(".control-group")

    picker = Fire.setup_deadline_picker(deadline_first, deadline_final)

    picker.validate()