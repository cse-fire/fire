$ ->
  updateDefaultRows = () ->
    $('table.table tbody:has(tr.default-row)').each (idx,tb) ->
      empty = $('tr:visible:not(.default-row)', tb).length == 0
      $('tr.default-row', tb).toggle(empty)
    for table in ['pending', 'rejected', 'accepted']
      trs = $("#table-" + table + "-submissions tr:visible")
      trs.filter(":odd").addClass("odd-row");
      trs.filter(":even").removeClass("odd-row");
      count = $("#table-" + table + "-submissions tbody tr:visible:not(.default-row)").length
      $("#" + table + "-submissions-count").attr('data-count', count).text(count)
  updateDefaultRows()

  updateHiddenRows = () ->
    show = $('#show-all-graders').hasClass('active')
    $('tr.other-grader').toggle(show)
    updateDefaultRows()

  $('#show-all-graders').click () ->
    setTimeout updateHiddenRows, 0

  $('[data-toggle=tooltip]').tooltip()