$ ->
  $('#loginform > input[name=email]').focus()
  signup_btn = $ '#signup-btn'
  signup_btn.click ->
    $('#signup-form-row').collapse('show')
    signup_btn.prop('disabled', true)
    $('#loginform input').prop('disabled', true)
    $('#loginform button').prop('disabled', true)
    $('#signupform input[name=signup_email]').focus()
  if window.location.hash == "#register"
    signup_btn.click()
    $('#signupform input[name=signup_email]').focus()

  resetpwd_btn = $ '#resetpwd-btn'
  resetpwd_btn.click ->
    $('#reset-password-form-row').collapse('show')
    $('#signup-btn').prop('disabled', true)
    $('#loginform input').prop('disabled', true)
    $('#loginform button').prop('disabled', true)
    $('#reset-password-form-row input[name=reset_email]').focus()