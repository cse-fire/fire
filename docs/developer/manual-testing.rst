==============
Manual testing
==============

Test cases
==========

.. _courseinit:

Course initialization
---------------------

**Pre-conditions**: none

==  ==============================================  ===========================
#   Step details                                    Expected results
==  ==============================================  ===========================
1   Add a test course to ``courses.ini``
2   Run ``fab deploy``                              Get an email
3   Click on the URL in the email                   Site should open
4   Log-in with the password provided in the email  Log-in successful
==  ==============================================  ===========================

.. _createlab:

Lab creation
------------

**Pre-conditions**: :ref:`courseinit`

==  ==============================================  ===========================
#   Step details                                    Expected results
==  ==============================================  ===========================
1   Log-in as a grader
2   Add an individual lab (only ``.pdf``, allow     Lab created
    physical submissions)
3   Add a group lab (Any file extension, min/max 2  Lab created
    members, no physical submissions)
4   Click on "Graders" (top), add yourself to all
    all labs
==  ==============================================  ===========================

.. _regstudents:

Student registration
--------------------

**Pre-conditions**: :ref:`courseinit`

==  ==============================================  ===========================
#   Step details                                    Expected results
==  ==============================================  ===========================
1   Enter an email                                  Get an email
2   Click link in email                             Registration form
3   Fill-in information                             Registration successful
4   Login with the newly created password           Success
==  ==============================================  ===========================

Do this twice

.. _submit1:

Submit (Student 1)
------------------

**Pre-conditions**: :ref:`createlab` :ref:`regstudents`

==  ==============================================  ===========================
#   Step details                                    Expected results
==  ==============================================  ===========================
1   Log-in as student 1
2   Go to lab 1
3   Upload a pdf file     Success
4   Upload a file which is not a pdf                Failure
5   Submit                                          Success
6   Go to lab 2                                     Can't submit without group
7   Go to home
8   Create group, record the join code
9   Go to lab 2                                     Can't submit: not enough
                                                    members in gruop
==  ==============================================  ===========================

.. _submit2:

Submit (Student 2)
------------------

**Pre-conditions**: :ref:`createlab` :ref:`regstudents`

==  ==============================================  ===========================
#   Step details                                    Expected results
==  ==============================================  ===========================
1   Log-in as student 2
2   Go to lab 1
3   Check the physical submission box
4   Submit                                          Success
5   Go to home
6   Join student 1's group                          Success
7   Go to lab 2
8   submit                                          Success
==  ==============================================  ===========================

.. _review:

Review
------

**Pre-conditions**: :ref:`submit1` :ref:`submit2`

==  ==============================================  ===========================
#   Step details                                    Expected results
==  ==============================================  ===========================
1   Log-in a sgrader                                Success, the bar shows 3
                                                    pending submissions
2   Go to lab 2                                     1 pending submission
                                                    assigned to you
3   Click on first
4   Reject                                          Both students should get a mail
5   Go to lab 1                                     2 pending submissions assigned to you
6   Click on the first
7   Accept                                          Student should get mail
8   Click on "register physical submissions"
9   Register physical submission for student 2      Student should get mail
10  Go to lab 1
11  Click on pending submission
12  Reject                                          Student should get mail
13  Go to lab 1
14  Click on register physical...
15  Register submission for student 2               Student should get mail
16  Got to lab 1
17  Click on pending submission
18  Accept                                          Student should get mail
==  ==============================================  ===========================
