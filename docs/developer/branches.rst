Git branches
============

The development model used for fire development is a model introduced by
Vincent Driessen in `A successful Git branching model
<http://nvie.com/posts/a-successful-git-branching-model/>`_.

All work is done on the ``develop`` branch, or a feature branch (which is then
merged into ``develop``). ``master`` should *always* be clean. To make a
release, You make a release branch form ``develop``, bumg the version and
commit any necessary bugfixes then you merge the release branch into master and
tag a release. Usually the only commit on the release branch is actually
bumping the version numbers. Have a look at the git tree view of the history.

Here is a graphical representation, courtesy Vincent Driessen (Creative Commons
BY-SA)

.. image:: git-branching-model.png
   :align: center
   :scale: 60%

