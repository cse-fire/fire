==========================
Frequently Asked Questions
==========================

How do I change group in the middle of a course?
================================================

It happens that one needs to change group in the middle of a course, that is
after having already completed some assignments.  To change group you must
first leave your current group by clicking on the "no entry" sign (⛔) beside
your group's number on the index page.  Once this is done, you can join an
existing group or create a new one.

.. caution::

   After changing group, you won't be able to submit to assignments that you
   already submitted in the old group.
   E.g. in a course with two labs A and B, if you submitted lab A as a member
   of group 1 and then leave group 1 and create a new group (group 2), you will
   not be able to submit to lab A anymore (lab B is fine).

   If you have labs that have not yet been accepted when changing group, we
   recommend that you **note the join code for your original group** to be
   able to switch back in case you need to re-submit.
