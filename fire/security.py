import hashlib
import os
from random import choice
from string import ascii_letters

from pyramid.security import unauthenticated_userid, Allow, Authenticated, DENY_ALL
from pyramid.authentication import AuthTktAuthenticationPolicy
from pyramid.authorization import ACLAuthorizationPolicy
from pyramid.httpexceptions import HTTPFound

from . import settings

"""Authentication and authorization handlers"""

import logging
log = logging.getLogger(__name__)


def includeme(config):
    """Configures the Pyramid app to use the appropriate authn/authz
    handlers."""
    log.debug("Configuring authn/authz policies.")

    policy = AuthTktAuthenticationPolicy(
        secret=settings.get(config, 'auth_tkt_secret'),
        callback=user_permissions,
        cookie_name='auth_tkt_' + settings.get(config, 'course_slug'),
        http_only=True,
        path='/' + settings.get(config, 'course_prefix'),
        debug=True
    )

    config.set_authentication_policy(policy)
    config.set_authorization_policy(ACLAuthorizationPolicy())
    config.set_root_factory(FireResource)
    config.set_default_permission('default_permission')
    # config.add_view(forbidden_view, context=Forbidden)

    config.add_request_method(get_user, 'user', property=True, reify=True)

    # Filter out cookies that we don't recognize. Mainly to prevent
    # untrusted clients from polluting the server's request.cookie.
    # WARNING: Do not disable this without reading the comments below
    # in the tween itself!
    config.add_tween('fire.security.filter_cookies_tween')


class FireResource(object):
    """A root factory for Pyramid's auth system"""
    def __init__(self, request):
        pass

    # TODO: this is a bit of a hack, just proxying 
    # permissions to principals.
    __acl__ = [(Allow, Authenticated, 'is:authenticated'),
               (Allow, 'role:student', 'has_role:student'),
               (Allow, 'role:grader', 'has_role:grader'),
               (Allow, 'role:admin', 'has_role:admin'),
               DENY_ALL]


def forbidden_view(request):
    return HTTPFound(location=request.route_path('login'))


def user_permissions(userid, request):
    log.debug("user_permissions called")
    from .models import User
    u = User.get(userid)
    if u: 
        return [Authenticated, 'role:' + u.role]


def get_user(request):
    log.debug("get_user called")
    userid = unauthenticated_userid(request)
    if userid is not None:
        from .models import User
        u = User.get(userid)
        log.debug("get_user returning %r", u)
        return u


def filter_cookies_tween(handler, registry):
    """This is a Pyramid Tween that ensures that no cookies are set
    in either requests or responses, except the auth and session tickets.
    It also makes sure Set-Cookie headers specify the path of the fire instance,
    to avoid leaking cookies between courses."""
    slug = settings.get(registry, 'course_slug')
    allowed_cookies = ('auth_tkt_' + slug,
                       'sess_tkt_' + slug)

    # WARNING: Please do not add cookie names to this list without conideration!
    # If you are running into this comment because you are modifying
    # Fire and added a cookie somewhere, then I strongly advise you to use
    # the session storage instead if possible. If you really need to share
    # data between server and the client, please use explicit passing through
    # template variables and get/post data. DO NOT trust data from the client!
    def tween(request):
        for key in request.cookies:
            if key not in allowed_cookies:
                del request.cookies[key]
        response = handler(request)
        # Since we are sending potentially sensitive data to the client, we will
        # explicitly *delete* cookies set in JS by setting them to empty strings.
        # This prevents e.g. attacks where script injections set cookies in one
        # session, and read them later in another session.
        # Simply put: Don't use cookies on the client at all.
        # TODO: finish this, find out what cookies are set in the response also
        #       It is possible that we must do this in a nginx filter
        # TODO: somehow force the browser to clear local storage (csp headers?)
        # as we don't want clients to remember *anything* between sessions
        return response
    return tween


def hash_pwd(pwd, salt=None, rounds=2**16):
    """ Hashes a password using ``rounds`` rounds of SHA512 and a 64-bit salt.
        The salt and hash result are concatenated and returned as a hexdigest.

        :param pwd: the password to hash
        :param salt: a 16 character hex string defining the salt.
        :param rounds: number of rounds to perform hashing. By default 2**16. Adjust
            this to slow down the hashing, but note that the nubmer of
            rounds is not stored in the output, so existing hashes will not
            work.

        :returns: A string of the form ``salt$alg$rounds$hash``, where salt and hash are
            hex strings, alg specifies the hashing algorithm and rounds is a decimal
            integer.
    """
    # Note: if this turns out to be too slow, consider using the py-bcrypt package
    if salt is None:
        # Note: Do not use python's random module for crypto!
        salt = os.urandom(8)
    else:
        salt = salt[:16].decode('hex')
    h = hashlib.sha512()
    r = rounds

    while r > 0:
        h.update(salt)
        # hashlib.sha512 only support bytestring arguments:
        # encode 'pwd' to utf-8 to support unicode characters in passwords
        # (some users put Swedish letters with umlauts into passwords)
        h.update(pwd.encode('utf_8'))
        r -= 1
    return '%s$%s$%d$%s' % (salt.encode('hex'), 'sha512', rounds, h.hexdigest())


def verify_pwd(pwd, hashed):
    """Verifies if a password matches a stored hash.

    :param pwd: plain-text password to test
    :param hashed: a hash generated by :func:`hash_pwd`
    :returns: ``True`` if and only iff pwd matches the hash, ``False`` otherwise
    """
    try:
        salt, alg, rounds, _ = hashed.split('$')
        rounds = int(rounds)
    except ValueError:
        raise ValueError("Supplied hash is not of the right form.")

    if alg != 'sha512':
        raise ValueError("Unknown algorithm specifier in hash.")

    return hash_pwd(pwd, salt=salt, rounds=rounds) == hashed


def random_hex_string(n):
    """Returns a random hex string of length n"""
    return os.urandom(n/2).encode('hex')


def pwgen(length=12):
    return ''.join(choice(ascii_letters) for _ in xrange(length))
