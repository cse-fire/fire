import os

from .models import Lab


def check_extension(filename, lab):
    assert isinstance(lab, Lab)
    _, ext = os.path.splitext(filename)
    if len(lab.allowed_file_extensions) < 1:
        return True
    else:
        return ext.lower() in [e.extension.lower() for e in lab.allowed_file_extensions]
