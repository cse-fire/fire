define("views/admin_graders", ['require', 'jquery'], function(require, $) {
  
$(function() {
  return $('#grader-workload .weight').change(function() {
    var grader, graders, lab, total;
    lab = $(this).parent().data('lab');
    grader = $(this).parent().data('grader');
    graders = $('#grader-workload td[data-lab=' + lab + ']');
    total = 0;
    graders.each(function() {
      var weight;
      weight = parseInt($(this).find("input").val());
      if (isNaN(weight)) {
        weight = 0;
        $(this).find("input").val(0);
      }
      return total += weight;
    });
    if (total > 0) {
      $('#lab-th-' + lab).removeClass('understaffed');
      graders.removeClass('understaffed');
    } else {
      $('#lab-th-' + lab).addClass('understaffed');
      graders.addClass('understaffed');
    }
    return graders.each(function() {
      var input, percentage, weight;
      input = $(this).find("input");
      weight = parseFloat(input.val());
      if (isNaN(weight)) {
        weight = 0;
        input.val(0);
      }
      percentage = total === 0 ? 0 : Math.round(100 * weight / total);
      $(this).val(weight);
      $(this).find("label").html(percentage);
      if (weight > 0) {
        return input.removeClass('unassigned');
      } else {
        return input.addClass('unassigned');
      }
    });
  });
});

});

define("views/grader_deadlines", ["jquery", "lib/fire"], function($) {
  return $(function() {
    var Fire;
    Fire = require("lib/fire");
    return $(".deadline-extension").each(function() {
      var final_oid, first_oid;
      first_oid = $(this).data('first-deadline-oid');
      final_oid = $(this).data('final-deadline-oid');
      return Fire.setup_deadline_picker($('#' + first_oid), $('#' + final_oid));
    });
  });
});
define("views/grader_groupsizes", ['require', 'jquery'], function(require, $) {
  
$(function() {
  $("#groupsizes form").each(function() {
    return deform.addCallback($(this)[0].id, function(oid) {
      var target;
      target = '#' + oid;
      return $(target).ajaxForm({
        target: target,
        dataType: 'json',
        beforeSubmit: function() {
          return $('body').css('cursor', 'wait');
        },
        success: function(response) {
          var td;
          td = $(target).parent();
          if (response.added) {
            td.removeClass('wrong-size');
          } else {
            if (td.data('wrongsize') === 1) {
              td.addClass('wrong-size');
            }
          }
          return $('body').css('cursor', 'default');
        },
        error: function() {
          return alert("FAIL");
        }
      });
    });
  });
  return $('#groupsizes input').change(function() {
    return $('#' + this.id + '_form').submit();
  });
});

});
define("views/grader_lab", ['require', 'jquery'], function(require, $) {
  
$(function() {
  var updateDefaultRows, updateHiddenRows;
  updateDefaultRows = function() {
    var count, table, trs, _i, _len, _ref, _results;
    $('table.table tbody:has(tr.default-row)').each(function(idx, tb) {
      var empty;
      empty = $('tr:visible:not(.default-row)', tb).length === 0;
      return $('tr.default-row', tb).toggle(empty);
    });
    _ref = ['pending', 'rejected', 'accepted'];
    _results = [];
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      table = _ref[_i];
      trs = $("#table-" + table + "-submissions tr:visible");
      trs.filter(":odd").addClass("odd-row");
      trs.filter(":even").removeClass("odd-row");
      count = $("#table-" + table + "-submissions tbody tr:visible:not(.default-row)").length;
      _results.push($("#" + table + "-submissions-count").attr('data-count', count).text(count));
    }
    return _results;
  };
  updateDefaultRows();
  updateHiddenRows = function() {
    var show;
    show = $('#show-all-graders').hasClass('active');
    $('tr.other-grader').toggle(show);
    return updateDefaultRows();
  };
  $('#show-all-graders').click(function() {
    return setTimeout(updateHiddenRows, 0);
  });
  return $('[data-toggle=tooltip]').tooltip();
});

});
define("views/grader_main", ['require', 'jquery'], function(require, $) {
  var SubmissionsGraph;

SubmissionsGraph = (function() {

  function SubmissionsGraph(canvas) {
    this.canvas = canvas;
    this.url = $(this.canvas).data('source');
    this.colorAll = $(this.canvas).data('color-all');
    this.colorMe = $(this.canvas).data('color-me');
    this.context = this.canvas.getContext("2d");
    this.refresh();
  }

  SubmissionsGraph.prototype.barWidth = 12;

  SubmissionsGraph.prototype.pxPerUnit = 5;

  SubmissionsGraph.prototype.drawBar = function(index, value) {
    return this.context.fillRect(this.canvas.width - (index + 1) * (this.barWidth + 1), this.canvas.height - value * this.pxPerUnit, this.barWidth, value * this.pxPerUnit);
  };

  SubmissionsGraph.prototype.draw = function(data) {
    var count, i, _i, _j, _len, _len1, _ref, _ref1, _results;
    this.context.fillStyle = this.colorAll;
    _ref = data.all;
    for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {
      count = _ref[i];
      this.drawBar(i, count);
    }
    this.context.fillStyle = this.colorMe;
    _ref1 = data.me;
    _results = [];
    for (i = _j = 0, _len1 = _ref1.length; _j < _len1; i = ++_j) {
      count = _ref1[i];
      _results.push(this.drawBar(i, count));
    }
    return _results;
  };

  SubmissionsGraph.prototype.refresh = function() {
    return $.ajax({
      url: this.url,
      dataType: "json",
      success: $.proxy(this.draw, this)
    });
  };

  return SubmissionsGraph;

})();

$(function() {
  var cva, _i, _len, _ref;
  _ref = $(".bars");
  for (_i = 0, _len = _ref.length; _i < _len; _i++) {
    cva = _ref[_i];
    new SubmissionsGraph(cva);
  }
  return $('span, small').tooltip();
});

});
define("views/grader_physical_overview", ['require', 'jquery'], function(require, $) {
  
$(function() {
  $('.register-button').click(function() {
    var row, submitter;
    $('tr').removeClass('info');
    $('.register-button').show().next().hide();
    row = $(this).closest('tr');
    row.addClass('info');
    $(this).hide().next().show();
    return submitter = row.data('submitter');
  });
  return $('.confirm-button').click(function() {
    var btn, form, row;
    btn = $(this);
    row = btn.closest('tr');
    form = $('#register-form');
    $('[name=submitter_id]', form).val(row.data('submitter'));
    btn.button('loading');
    return $.post(form.attr('action'), form.serialize(), function(data) {
      console.log(data);
      row.removeClass('info');
      if (data.error) {
        return btn.parent().html("<span class='text-error'>Error: " + data.error + "</span>");
      } else {
        return btn.parent().html("<span class='text-success'>Registered!</span>");
      }
    });
  });
});

});
define("views/grader_students", ['require', 'jquery'], function(require, $) {
  
$(function() {
  return $('td .icon').tooltip();
});

});

define("views/grader_submissions", ["jquery", "lib/markdown_preview"], function($) {
  return $(function() {
    var Markdown;
    Markdown = require("lib/markdown_preview");
    $('[data-toggle=tooltip]').tooltip();
    $('.fileaction').each(function() {
      var $this, file_url, filename, subid;
      $this = $(this);
      subid = $this.closest('.submission').data('submission');
      filename = $this.closest('tr').data('filename');
      file_url = '../../../../download/' + subid + '/' + encodeURIComponent(filename);
      switch ($this.data('action')) {
        case "download":
          return $this.click(function() {
            return window.location.href = file_url + "?dl=1";
          });
        case "browse":
          return $this.attr('href', file_url);
      }
    });
    $('#reassign-grader select').change(function() {
      return $('#reassign-grader').submit();
    });
    $('.check-output-show-full').click(function() {
      $(this).parent().removeClass('truncated');
      return $(this).hide();
    });
    $('.markdown').each(function(i, m) {
      return Markdown.setup_markdown_previewer({
        source: $(m).find('.markdown-source'),
        preview: $(m).find('.markdown-preview'),
        toggle: $(m).find('.markdown-toggle')
      });
    });
    $('.submission-review-form').each(function(i, f) {
      var autosave, autosave_interval, autosave_lastval, autosave_ongoing, autosave_time, form, statusbar;
      form = $(f);
      statusbar = $(f).find('.submission-status');
      if (!form.length) {
        return;
      }
      autosave_interval = 3000;
      autosave_lastval = form.serialize();
      autosave_time = -1;
      autosave_ongoing = false;
      autosave = function() {
        var newdata, now;
        newdata = form.serialize();
        if (!autosave_ongoing && newdata !== autosave_lastval) {
          now = new Date();
          if (now.getTime() - autosave_time > autosave_interval) {
            $.post(form.data('autosave-url'), newdata, function(data) {
              if (data.error) {
                return statusbar.text('Saving failed: ' + data.error);
              } else {
                autosave_lastval = newdata;
                autosave_time = now.getTime();
                return statusbar.text('Saved at ' + now.toLocaleTimeString());
              }
            });
          }
        }
        return setTimeout(autosave, autosave_interval);
      };
      return autosave();
    });
    $('.submission .minimizer').click(function() {
      var submission;
      submission = $(this).parent();
      submission.addClass("submission-minimized");
      return false;
    });
    return $('.submission').click(function() {
      if ($(this).hasClass("submission-minimized")) {
        return $(this).removeClass("submission-minimized");
      }
    });
  });
});

define("views/lab_settings", ["jquery", "lib/fire"], function($) {
  return $(function() {
    var Fire, deadline_final, deadline_first, picker;
    Fire = require("lib/fire");
    deadline_first = $('input[name="first_deadline"]').parents(".control-group");
    deadline_final = $('input[name="final_deadline"]').parents(".control-group");
    picker = Fire.setup_deadline_picker(deadline_first, deadline_final);
    return picker.validate();
  });
});
define("views/login", ['require', 'jquery'], function(require, $) {
  
$(function() {
  var resetpwd_btn, signup_btn;
  $('#loginform > input[name=email]').focus();
  signup_btn = $('#signup-btn');
  signup_btn.click(function() {
    $('#signup-form-row').collapse('show');
    signup_btn.prop('disabled', true);
    $('#loginform input').prop('disabled', true);
    $('#loginform button').prop('disabled', true);
    return $('#signupform input[name=signup_email]').focus();
  });
  if (window.location.hash === "#register") {
    signup_btn.click();
    $('#signupform input[name=signup_email]').focus();
  }
  resetpwd_btn = $('#resetpwd-btn');
  return resetpwd_btn.click(function() {
    $('#reset-password-form-row').collapse('show');
    $('#signup-btn').prop('disabled', true);
    $('#loginform input').prop('disabled', true);
    $('#loginform button').prop('disabled', true);
    return $('#reset-password-form-row input[name=reset_email]').focus();
  });
});

});

define("views/new_lab", ["jquery", "lib/fire"], function($) {
  return $(function() {
    var Fire, deadline_final, deadline_first, picker;
    Fire = require("lib/fire");
    deadline_first = $('input[name="first_deadline"]').parents(".control-group");
    deadline_final = $('input[name="final_deadline"]').parents(".control-group");
    return picker = Fire.setup_deadline_picker(deadline_first, deadline_final);
  });
});

define("views/student_lab", ["jquery", "jquery.fileupload", "lib/markdown_preview"], function($) {
  return $(function() {
    var Markdown, activeUploads, newfiles, setupFileActions;
    $('[data-toggle=tooltip]').tooltip();
    Markdown = require("lib/markdown_preview");
    activeUploads = 0;
    newfiles = false;
    $("#fileupload").fileupload({
      dataType: "json",
      add: function(e, data) {
        ++activeUploads;
        $('#submit-button').button('loading');
        return data.submit();
      },
      submit: function(e, data) {
        var td, tr;
        tr = $('<tr/>');
        td = $('<td class="filename"/>');
        console.log(data.files);
        td.text(data.files[0].name);
        tr.append(td);
        tr.append("<td/>");
        tr.append('<td colspan="2" class="message"><div class="progress"><div class="bar" style="width: 0%;"></div></div></td>');
        data.tableRow = tr;
        return $('#filelisting tbody').append(tr);
      },
      done: function(e, data) {
        var newrow;
        if (data.result.error) {
          return $('.message', data.tableRow).addClass('message-error').text(data.result.error);
        } else {
          newrow = $(data.result.tablerow);
          setupFileActions(newrow);
          $(data.tableRow).replaceWith(newrow);
          if (--activeUploads === 0) {
            $('#submit-button').button('reset');
          }
          return newfiles = true;
        }
      },
      fail: function(e, data) {
        return $('.message', data.tableRow).addClass('message-error').text("Upload failed: " + data.jqXHR.statusText);
      },
      progress: function(e, data) {
        var bar, progress;
        progress = parseInt(data.loaded / data.total * 100, 10);
        bar = $(".progress .bar", data.tableRow).css('width', progress + '%');
        if (progress >= 95) {
          return bar.addClass("progress-striped active");
        }
      }
    });
    $('#browse-button').click(function() {
      return $('#fileupload').click();
    });
    $('.modal-cancel').click(function() {
      return $(this).closest('.modal').modal('hide');
    });
    setupFileActions = function(tr) {
      var $tr;
      $tr = $(tr);
      return $('.fileaction', tr).click(function() {
        var action, filename, mdl, subid;
        action = $(this).data('action');
        filename = $tr.data('filename');
        switch (action) {
          case "delete":
            mdl = $('#modal-confirm-delete');
            mdl.find('#confirm-delete-filename').text(filename);
            mdl.find('#confirm-delete-button').unbind('click').click(function() {
              var url;
              mdl.modal('hide');
              url = $(this).data('url');
              return $.post(url, filename, function(data) {
                var tbody;
                if (data.error) {
                  return alert(data.error);
                } else {
                  tbody = $tr.parent();
                  return $tr.slideUp(function() {
                    return $tr.remove();
                  });
                }
              });
            });
            mdl.modal();
            break;
          case 'download':
            subid = $tr.closest('.submission').data('submission');
            return window.location.href = '../download/' + subid + '/' + encodeURIComponent(filename) + '?dl=1';
          case 'download-draft':
            return window.location.href += '/draft/files/' + encodeURIComponent(filename) + '?dl=1';
        }
      });
    };
    $('.filelisting-5col tr').each(function(i, tr) {
      return setupFileActions(tr);
    });
    $('.check-output-show-full').click(function() {
      $(this).parent().removeClass('truncated');
      return $(this).hide();
    });
    $('.markdown').each(function(i, m) {
      return Markdown.setup_markdown_previewer({
        source: $(m).find('.markdown-source'),
        preview: $(m).find('.markdown-preview'),
        toggle: $(m).find('.markdown-toggle')
      });
    });
    $('#submit-button').click(function() {
      var mdl, p;
      mdl = $('#modal-confirm-submit');
      $('input[name=comment]').val($('#comment_edit_field').val());
      if ($('#grader')) {
        $('input[name=grader]').val($('#grader').val());
      }
      p = $('#physical_submission');
      if (p.length && p.is(':checked')) {
        $('input[name=physical]').val('yes');
      }
      return mdl.modal();
    });
    $(window).bind('beforeunload', function() {
      if (newfiles) {
        return 'Uploaded files are not considered submitted until you press the submit button.';
      }
    });
    $('#confirm-submit-button').click(function() {
      newfiles = false;
      return true;
    });
    $('.submission .minimizer').click(function() {
      var submission;
      submission = $(this).parent();
      submission.addClass("submission-minimized");
      return false;
    });
    return $('.submission').click(function() {
      if ($(this).hasClass("submission-minimized")) {
        return $(this).removeClass("submission-minimized");
      }
    });
  });
});
define("views/student_main", ['require', 'jquery'], function(require, $) {
  
$(function() {
  $('.lab h2').click(function() {
    return $('a', this).click();
  });
  $('a[data-reveal]').click(function() {
    var $this;
    $this = $(this);
    return $this.replaceWith("<code>" + $this.data('reveal') + "</code>");
  });
  $('#modal-newgroup input[name=joincode]').keyup(function() {
    if ($(this).val().length > 0) {
      return $('#confirm-newgroup').text('Join group');
    } else {
      return $('#confirm-newgroup').text('Create group');
    }
  });
  $('#confirm-newgroup').click(function() {
    return $('#modal-newgroup form').submit();
  });
  $('#confirm-leavegroup').click(function() {
    if (confirm("Are you sure you want to leave the group?")) {
      return $('#leavegroup form').submit();
    }
  });
  return $('[data-toggle=tooltip]').tooltip();
});

});
