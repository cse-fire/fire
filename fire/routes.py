from .models import model_resource, User


# Route predicates
def not_logged_in(_, request):
    return request.user is None


def has_role(role):
    def predicate(_, request):
        return request.user is not None and request.user.role == role

    return predicate


# Main route config picked up by config.scan
def includeme(config):
    # Main page, chosen based on permission predicates on the views
    config.add_route('student_main', '/', effective_principals="role:student")
    config.add_route('grader_main', '/', effective_principals="role:grader")
    config.add_route('root', '/')  # This one must be last, as it has __no_permission_required__

    # Generic pages
    # config.add_route('about', '/about')
    config.add_route('logout', '/logout')
    config.add_route('login', '/login')
    config.add_route('signup', '/login/new')
    config.add_route('verify_email', '/login/verify/{code}')
    config.add_route('password_reset', '/login/resetpwd')
    config.add_route('user_profile', '/profile')
    config.add_route('update_user_info', '/profile/info/save')  # POST only
    config.add_route('user_password', '/profile/password')
    config.add_route('update_user_password', '/profile/password/save')  # POST only
    config.add_route('markdown_guide', '/docs/markdown')
    config.add_route('markdown_render', '/util/markdown')  # POST only

    # Serving of files with issued FileCaps
    config.add_route('get_filecap', '/filecap/{filecap_id}/{filename}')
    config.add_route('download_file', '/download/{submission}/{filename}')

    # Control center (for master login when system is deactivated)
    # config.add_route('cc_main', '/cc')
    # config.add_route('cc_login', '/cc/login')
    # config.add_route('cc_admins', '/cc/admins')
    # config.add_route('cc_labs', '/cc/labs')
    # config.add_route('cc_editlab', '/cc/labs/{lab}')
    # config.add_route('cc_settings', '/cc/settings')
    # config.add_route('cc_deactivate', '/cc/deactivate')

    # Student pages
    # config.add_route('students_joingroup', '/joingroup')
    # config.add_route('students_leavegroup', '/leavegroup')
    config.add_route('student_lab', '/labs/{lab:\d+}')
    config.add_route('student_upload', '/labs/{lab}/upload')  # json
    config.add_route('student_delete_file', '/labs/{lab}/delete_file')  # json
    config.add_route('student_download_draft_file', '/labs/{lab}/draft/files/{filename}')
    config.add_route('student_submit', '/labs/{lab}/submit')
    config.add_route('student_newgroup', '/create_group')
    config.add_route('student_leavegroup', '/leave_group')
    # config.add_route('students_downloadfile', '/labs/{lab{/files/{file}')

    # Teacher pages (general)
    config.add_route('admin_graders', '/admin/graders')
    # config.add_route('admin_main', '/admin/dashboard')
    # config.add_route('admin_lab', '/admin/lab/{lab}')
    # config.add_route('admin_setstatus', '/admin/XX') TODO: what was this?
    # config.add_route('admin_students', '/admin/students')
    # config.add_route('admin_student', '/admin/students/{studentid}')
    # config.add_route('admin_groups', '/admin/groups')
    # config.add_route('admin_group', '/admin/groups/{groupid}')

    # Grader pages (lab management / grading)
    config.add_route('new_lab', '/admin/labs/new')
    config.add_route('grader_lab', '/admin/labs/{lab}')
    config.add_route('lab_settings', '/admin/labs/{lab}/settings')
    config.add_route('lab_delete', '/admin/{lab}/delete')  # json
    config.add_route('lab_checks', '/admin/labs/{lab}/checks')
    config.add_route('grader_download_pending', '/admin/labs/{lab}/getpending')
    config.add_route('grader_download_all', '/admin/labs/{lab}/getall')
    config.add_route('grader_physical_overview', '/admin/labs/{lab}/physical')
    config.add_route('grader_register_physical', '/admin/labs/{lab}/physical/register')
    config.add_route('grader_submissions', '/admin/labs/{lab}/submissions/{submitter}')
    config.add_route('grader_save_draft', '/admin/submissions/{submission}/draft')
    config.add_route('grader_post_review', '/admin/submissions/{submission}/review')
    config.add_route('grader_download_submission', '/admin/submissions/{submission}/download')
    # config.add_route('admin_uploadfile', '/admin/lab/{lab}/submissions/{submission}/upload')
    config.add_route('grader_submissions_reassign', '/admin/submission{submission}/reassign')
    config.add_route('graph_submissions', '/admin/lab/{lab}/graphs/submissions')
    config.add_route('grader_recheck','/admin/recheck')
    config.add_route('grader_run_check','/admin/runcheck')

    # Grader pages (student and group administration)
    config.add_route('grader_students', '/admin/students')
    config.add_route('grader_unverified', '/admin/unverified')
    config.add_route('grader_resets', '/admin/resets')
    config.add_route('grader_students_csv', '/admin/students/csv')
    config.add_route('grader_view_student', '/admin/students/{student_id}',
                     factory=model_resource(User, match='student_id'))
    config.add_route('grader_edit_student', '/admin/students/{student_id}/save',
                     factory=model_resource(User, match='student_id'))
    config.add_route('grader_impersonate', '/admin/students/{student_id}/impersonate',
                     factory=model_resource(User, match='student_id'))

    config.add_route('grader_deadline_extensions', '/admin/deadlines')
    config.add_route('grader_new_deadline_extension', '/admin/deadlines/new')
    config.add_route('grader_update_deadline_extension', '/admin/deadlines/update')

    config.add_route('grader_group_size_exceptions', '/admin/groupsizes')
    config.add_route('grader_update_group_size_exception', '/admin/groupsizes/update')

    config.add_route('settings', '/settings')

    # Grader profile
    config.add_route('grader_notifications', '/profile/notifications')
    config.add_route('update_grader_notifications', '/profile/notifications/save')  # POST only

    # Check API
    config.add_route('check_feedback', '/check/{token}/')

    # Reports and utils
    # config.add_route('admin_viewlog', '/admin/reports/activity')
    # config.add_route('admin_coursereport', '/admin/reports/course')
    # config.add_route('admin_emaillist', '/admin/email')

    # config.add_route('admin_su', '/admin/dashboard')  TODO implement site-wide
    # config.add_route('admin_extdownload_browse', '/admin/dashboard')  TODO what are these?
    # config.add_route('admin_extdownload_download', '/admin/dashboard')

    # In order to use route_url to generate static urls in templates, we need
    # to add the following route. static=True means it won't be used in routing itself.
    config.add_route('static', '/static/*file', static=True)
    config.add_static_view('static', 'static', cache_max_age=3600)
