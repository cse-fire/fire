import os
from datetime import date, timedelta
from pyramid.response import Response

from pyramid.view import view_config, view_defaults
from pyramid.httpexceptions import HTTPFound, HTTPNotFound
from pyramid.security import remember

from sqlalchemy import text

from .. import filestore, mail, settings
from ..models import *
from ..forms import deform, make_form, SubmissionsGraderSchema, AddStudentSchema, GraderNotificationsSchema
from ..schemata import StudentSettingsSchema
from ..filestore import files_for_submission
from ..tasks import filecap_url

import urllib2
import threading


import csv
from datetime import datetime

import logging

log = logging.getLogger(__name__)


@view_config(route_name='grader_main',
             renderer='fire:templates/grader_main.html',
             permission='has_role:grader')
def grader_main(request):
    labs = Lab.get_active()

    lab_info = list()
    for lab in labs:
        pending = lab.pending_submissions
        stat = dict(
            own=len([s for s in pending if s.assigned_grader == request.user]),
            pending=len(pending),
            rejected=len(lab.rejected_submissions),
            accepted=len(lab.accepted_submissions),
            physical_waiting=len([s for s in pending
                                  if s.physical_submission and s.physical_submission_received_date is None])
        )

        lab_info.append(dict(lab=lab, stats=stat))

    return dict(labinfo=lab_info)


@view_config(route_name='grader_students',
             renderer='fire:templates/grader_students.html',
             permission='has_role:grader')
def grader_students(request):
    students = Student.get_all()
    labs = Lab.get_active()
    summaries = {(s.email, l.id): l.deciding_submission_for_submitter(s) for s in students for l in labs}
    return dict(
        students=students,
        groups=Group.get_all(),
        labs=labs,
        summaries=summaries,
        unverified_emails=EmailVerification.get_unverified(),
        unfinished_pwd_resets=DBSession.query(PasswordReset).all()
    )


@view_config(route_name='grader_unverified',
             renderer='fire:templates/grader_unverified.html',
             permission='has_role:grader')
def grader_unverified(request):
    return dict(
        students=Student.get_all(),
        unverified_emails=EmailVerification.get_unverified(),
        unfinished_pwd_resets=DBSession.query(PasswordReset).all()
    )


@view_config(route_name='grader_resets',
             renderer='fire:templates/grader_resets.html',
             permission='has_role:grader')
def grader_resets(request):
    return dict(
        students=Student.get_all(),
        unverified_emails=EmailVerification.get_unverified(),
        unfinished_pwd_resets=DBSession.query(PasswordReset).all()
    )


@view_config(route_name='grader_students_csv',
             permission='has_role:grader')
def grader_students_csv(request):
    students = Student.get_all('course', text('last_name COLLATE NOCASE'))

    response = Response(content_type='text/plain; charset=utf-8')
    response.content_disposition = 'attachment; filename="%s.csv"' % settings.get(request, 'course_slug')

    writer = csv.writer(response)

    def status(submission):
        if not submission:
            return '-'

        if submission.review:
            review = submission.review

            if review.grade:
                return review.grade

            if review.decision == Review.Decision.accepted:
                return '+'

        return '-'

    labs = Lab.get_active()
    for student in students:
        row = [student.last_name, student.first_name,
               student.email, student.id_number,
               student.course, str(student.group.id) if student.group else '-']
        submissions = [lab.deciding_submission_for_submitter(student) for lab in labs]
        row += map(status, submissions)
        writer.writerow([col.encode('utf-8') for col in row])

    return response


@view_config(route_name='grader_lab',
             renderer='fire:templates/grader_lab.html',
             permission='has_role:grader')
def grader_lab(request):
    lab = Lab.get_or_404(int(request.matchdict['lab']))
    return dict(
        lab=lab,
        pending=lab.pending_submissions,
        rejected=lab.rejected_submissions,
        accepted=lab.accepted_submissions
    )


@view_config(route_name='grader_download_pending',
             permission='has_role:grader')
def grader_download_pending(request):
    lab = Lab.get_or_404(int(request.matchdict['lab']))
    root_dir = "pending_submissions_%s_%s" % (settings.get(request, 'course_slug'), lab.slug if lab.slug else lab.id)

    filelisting = []
    pending = (s for s in lab.pending_submissions if s.assigned_grader == request.user)
    for sub in pending:
        refresh_submission_info(sub)
        submitter_dir = sub.submitter.slug
        _collect_files_for_sub(filelisting, sub, os.path.join(root_dir, submitter_dir))

        # Find older submissions as well
        submitter = sub.submitter
        othersubs = (s for s in submitter.submissions_for_lab(lab) if s.review)
        for othersub in othersubs:
            refresh_submission_info(othersub)
            _collect_files_for_sub(filelisting, othersub,
                                   os.path.join(root_dir, submitter_dir, 'submission%02d' % othersub.number))

    return download_as_archive(request, root_dir, filelisting)


def refresh_submission_info(sub):
    def submission_info(sub):
        yield "submitted on %s.\n%s\n\n" % (sub.date, sub.comment.source() if sub.comment else "")
        for r in sub.reviews:
            yield "review on %s:\n%s\n\n" % (r.date, r.review.source() if r.review else "")

    filestore.store_info(sub, "".join(submission_info(sub)))


@view_config(route_name='grader_download_all',
             permission='has_role:grader')
def grader_download_all(request):
    lab = Lab.get_or_404(int(request.matchdict['lab']))
    root_dir = "all_submissions_%s_%s" % (settings.get(request, 'course_slug'), lab.slug if lab.slug else lab.id)

    filelisting = []
    for sub in lab.submissions:
        refresh_submission_info(sub)
        submitter_dir = sub.submitter.slug
        _collect_files_for_sub(filelisting, sub,
                               os.path.join(root_dir, submitter_dir, "submission%02d" % sub.number))

    return download_as_archive(request, root_dir, filelisting)


def download_as_archive(request, root_dir, filelisting):
    cap = FileCap.issue_archive(filelisting, root_dir, 600)

    cap_path = request.route_path('get_filecap', filecap_id=cap.id, filename=cap.filename)
    cap_url = settings.get(request, 'url_capserver') + cap_path

    return HTTPFound(cap_url)


def _collect_files_for_sub(filelisting, sub, prefix):
    subfiles = filestore.files_for_submission(sub, reserved = True)
    for f in subfiles['files']:
        filelisting.append((  # Note that this is a pair
                              filestore.fullpath_for_submission_file(sub, f.path),
                              os.path.join(prefix, f.name)))
    for f in subfiles['deleted_files']:
        filelisting.append((  # Note that this is a pair
                              filestore.fullpath_for_submission_file(sub, f['path']),
                              os.path.join(prefix, '__deleted', f['name'])))


@view_config(route_name='grader_physical_overview',
             renderer='fire:templates/grader_physical_overview.html',
             permission='has_role:grader')
def grader_physical_overview(request):
    lab = Lab.get_or_404(int(request.matchdict['lab']))

    if not lab.allow_physical_submissions:
        request.flash_fire(type='error', msg=("Lab %s does not allow physical submissions." % lab.slug))
        raise HTTPFound(request.route_path('grader_lab', lab=lab.id))

    if lab.individual:
        submitters = Student.get_all()
    else:
        submitters = Group.get_all()  # TODO test

    pending_submissions = [submitter.pending_submission(lab) for submitter in submitters]

    return dict(lab=lab, submitters=zip(submitters, pending_submissions),
                csrf_token=request.session.get_csrf_token())


@view_config(route_name='grader_register_physical',
             request_method='POST',
             renderer='json',
             check_csrf=True,
             permission='has_role:grader')
def grader_register_physical(request):
    lab = Lab.get_or_404(int(request.matchdict['lab']))

    if lab.individual:
        submitter = User.get(request.POST.getone('submitter_id'))
    else:
        submitter = Group.get(int(request.POST.getone('submitter_id')))

    pending_submission = submitter.pending_submission(lab)

    if pending_submission:
        submission = pending_submission
        submission.physical_submission = True
    else:
        submission = Submission(lab, submitter, None, True)
        submission.assign_grader()

    if submission is None:
        return dict(error="Couldn't find or open a new submission.")

    if submission.physical_submission_received_date is not None:
        return dict(error="Physical submission is already registered.")

    submission.physical_submission_received_by = request.user
    submission.physical_submission_received_date = datetime.now()

    # We skip the deadline check here, since it is a grader performing this op
    # TODO: warn the grader if the deadline has passed.

    mail.sendmail(request, [u.email for u in submission.submitters],
                  "Your physical submission has been received",
                  'fire:templates/mails/notify_physical_registration.txt',
                  dict(submission=submission,
                       user=request.user.safe_info,
                       submitter_name=submitter.name,
                       course_name=Settings.get('course_name')))

    return dict(success=True)


def grader_form(request):
    return lambda submission: make_form(
        SubmissionsGraderSchema().bind(graders=Grader.get_all()),
        action=request.route_path('grader_submissions_reassign', submission=submission),
        formid='reassign-grader',
        use_ajax=True
    )


def add_submitter_form(request):
    return lambda submission: make_form(
        AddStudentSchema().bind(students=Student.get_all()),
        action=request.route_path('grader_submissions_reassign', submission=submission),
        formid='reassign-grader',
        use_ajax=True
    )


@view_config(route_name='grader_submissions',
             renderer='fire:templates/grader_submissions.html',
             permission='has_role:grader')
def grader_submissions(request):
    lab = Lab.get_or_404(int(request.matchdict['lab']))

    submitter_id = request.matchdict['submitter']

    if lab.individual:
        submitter = User.get_or_404(submitter_id)
    else:
        group_id = submitter_id
        if group_id.startswith('group'):
            group_id = int(group_id[5:])
        else:
            return HTTPNotFound()
        submitter = Group.get_or_404(group_id)

    submitter_history = [(l, l.deciding_submission_for_submitter(submitter)) for l in Lab.get_active()]

    deadlines = lab.get_deadlines(submitter, full_info=True)

    submissions = sorted(submitter.submissions_for_lab(lab), key=lambda s: s.date, reverse=True)
    submitted = [(s, ReviewDraft.fetch(s), filestore.files_for_submission(s)) for s in submissions]

    if not submitted:
        return HTTPNotFound()

    return dict(
        lab=lab,
        submitter=submitter,
        submitter_history=submitter_history,
        submitted=submitted,
        deadlines=deadlines,
        grader_form=grader_form(request),
        add_submitter_form=add_submitter_form(request),
        csrf_token=request.session.get_csrf_token()
    )

def run_check(request,submission,job):

    files = files_for_submission(submission)
    cap = FileCap.issue_archive(
        [(f.path, f.name) for f in files['files']], 'submission.tgz', 3600)
    submission_url = filecap_url(request, cap)
    feedback_url = request.route_url('check_feedback', token=job.token)
    job_url = job.check.get_url(submission_url, feedback_url)

    job.status = job.PENDING;

    log.debug("Running automated check '%s' on URL '%s'" % (job.check.name, job_url))
    threading.Thread(target=urllib2.urlopen, args=(job_url,), kwargs={}).start()

@view_config(route_name='grader_run_check',
             request_method='POST',
             permission='has_role:grader')
def grader_run_recheck(request):
    submission = Submission.get(int(request.params['submission']))
    check = CheckHook.get_or_404((request.params['check'],request.params['lab']))
    job = CheckResult(submission=submission, check=check)

    if submission is None:
        log.debug("This submission does not exists: '%s'" % request.matchdict['submission'])

    run_check(request,submission,job)

    return HTTPFound(location=request.route_path('grader_submissions',lab=submission.lab.id,submitter=submission.submitter.slug))

@view_config(route_name='grader_recheck',
             request_method='POST',
             permission='has_role:grader')
def grader_recheck(request):
    submission = Submission.get(int(request.params['submission']))
    job = CheckResult.get(request.params['check'])

    if submission is None:
        log.debug("This submission does not exists: '%s'" % request.matchdict['submission'])

    if job is None:
        log.debug("This token does not exists: '%s'" % request.matchdict['token'])

    run_check(request,submission,job)

    return HTTPFound(location=request.route_path('grader_submissions',lab=submission.lab.id,submitter=submission.submitter.slug))


@view_config(route_name='grader_download_submission',
             permission='has_role:grader')
def grader_download_submission(request):
    submission = Submission.get_or_404(int(request.matchdict['submission']))
    root_dir = "%s_%s" % (submission.lab.slug, submission.submitter.slug)

    filelisting = []
    refresh_submission_info(submission)
    _collect_files_for_sub(filelisting, submission, root_dir)

    return download_as_archive(request, root_dir, filelisting)


@view_config(route_name='grader_save_draft',
             request_method='POST',
             renderer='json',
             check_csrf=True,
             permission='has_role:grader')
def grader_save_draft(request):
    submission = Submission.get_or_404(int(request.matchdict['submission']))
    grader = request.user

    grade = request.POST.getone('grade')
    note = request.POST.getone('note')
    review = Markdown(request.POST.getone('review'))

    draft = ReviewDraft.fetch(submission)
    if not draft:
        draft = ReviewDraft(submission, grader, grade, note, review)
        DBSession().add(draft)
    else:
        draft.date = datetime.now()
        draft.grader = grader
        draft.grade = grade
        draft.note = note
        draft.review = review

    return dict(success=True)


@view_config(route_name='grader_post_review',
             request_method='POST',
             check_csrf=True,
             renderer='json',
             permission='has_role:grader')
def grader_post_review(request):
    submission = Submission.get_or_404(int(request.matchdict['submission']))
    lab = submission.lab

    accept = request.POST.getone('accept') == 'yes'
    grade = request.POST.getone('grade')
    note = request.POST.getone('note')
    review = Markdown(request.POST.getone('review'))

    submission.post_review(accept, request.user, grade=grade, note=note, review=review)

    draft = ReviewDraft.fetch(submission)
    if draft:
        DBSession().delete(draft)

    subject_pattern = "Your submission to \"%s\" was " + ("accepted" if accept else "rejected")
    subject = subject_pattern % lab.title

    submitter = submission.submitter

    context = dict(submission=submission,
                   user=request.user.safe_info,
                   submitter_name=submitter.name,
                   accepted=accept,
                   review=review.source(),
                   course_name=Settings.get('course_name'))

    recipients = [u.email for u in submission.submitters]

    # What we want to do is to include grader's name and email
    # in review notification so that a student could reply to it,
    # effectively replying to the grader. This SO answer:
    #
    # http://stackoverflow.com/a/14555043/1344648
    #
    # suggests the following format of headers:
    #
    #   From: John Doe via Fire <grader@fire.cse.chalmers.se>
    #   Sender: noreply@fire.cse.chalmers.se
    #   Reply-To: John Doe <John.Doe@gmail.com>
    sender = u'%s via Fire <graders-noreply@fire.cse.chalmers.se>' % request.user.name
    extra_headers = {'Sender': 'noreply@fire.cse.chalmers.se',
                     'Reply-To': u"%s <%s>" % (request.user.name, request.user.email)}

    mail.sendmail(request, recipients, subject,
                  'fire:templates/mails/notify_review.txt', context,
                  sender=sender, extra_headers=extra_headers)

    return HTTPFound(request.route_path('grader_lab', lab=lab.id))


@view_config(route_name='grader_submissions_reassign',
             request_method='POST', check_csrf=True,
             renderer='genshistream',
             permission='has_role:grader')
def grader_submissions_reassign(request):
    submission = Submission.get_or_404(int(request.matchdict['submission']))

    form = grader_form(request)(submission.id)

    try:
        data = form.validate(request.POST.items())
    except deform.ValidationFailure, e:
        return e.render()

    grader = Grader.get_or_404(data['grader_id'])

    if submission.assigned_grader:
        DBSession.query(GraderAssignment).filter(GraderAssignment.submission_id == submission.id).delete()

    submission.assign_grader(grader)

    return form.render(dict(grader_id=grader.id))


@view_config(route_name='grader_impersonate',
             request_method='POST', check_csrf=True,
             permission='impersonate')
def grader_impersonate_activate(context, request):
    user = context
    headers = remember(request, user.email)
    request.session.invalidate()
    request.flash_fire(type='success', msg="You are now logged in as %s. Remember to logout when done." % user.email)
    return HTTPFound(location=request.route_path('root'), headers=headers)


@view_config(route_name='grader_view_student',
             renderer='fire:templates/grader_view_student.html',
             permission='view')
def grader_view_student(student, request):
    course_codes = [cc.code for cc in CourseCode.get_all()]
    form = make_form(StudentSettingsSchema().bind(course_codes=course_codes),
                     buttons=('Update information',),
                     action=request.route_path('grader_edit_student', student_id=student.id),
                     formid='settingsform',
                     use_ajax=True)
    data = {k: getattr(student, k) for k in ('first_name', 'last_name', 'id_number', 'course')}
    form.set_appstruct(data)

    labs = Lab.get_active()
    deadline_extensions = filter(None, [l.get_deadline_extension(student) for l in labs])
    submissions = filter(None, [l.deciding_submission_for_submitter(student) for l in labs])

    return dict(form=form, student=student, submissions=submissions, deadline_extensions=deadline_extensions,
                Lab=Lab,
                csrf_token=request.session.get_csrf_token())


@view_config(route_name='grader_edit_student',
             renderer='genshistream',
             request_method='POST', check_csrf=True,
             permission='edit')
def grader_edit_student(student, request):
    course_codes = [cc.code for cc in CourseCode.get_all()]
    form = make_form(StudentSettingsSchema().bind(course_codes=course_codes),
                     buttons=('save',),
                     action=request.route_path('grader_edit_student', student_id=student.id),
                     formid='settingsform',
                     use_ajax=True)
    try:
        data = form.validate(request.POST.items())
    except deform.ValidationFailure, e:
        return e.render()

    # Update the student
    u = student
    for k in ('first_name', 'last_name', 'id_number', 'course'):
        if k in data:
            setattr(u, k, data[k])

    data = {k: getattr(student, k) for k in ('first_name', 'last_name', 'id_number', 'course')}

    return form.render(data, successmsg='Saved!')


@view_config(route_name='grader_deadline_extensions',
             request_method='GET',
             renderer='fire:templates/grader_deadlines.html',
             permission='has_role:grader')
def grader_deadline_extensions(request):
    labs = Lab.get_active()
    groups = Group.get_all()
    students = Student.get_all()
    return dict(labs=labs, groups=groups, students=students,
                deadline_extensions=len(GroupDeadlineExtension.get_all()) + len(IndividualDeadlineExtension.get_all()),
                groupsize_exceptions=len(GroupSizeException.get_all()))


@view_config(route_name='grader_new_deadline_extension',
             request_method='POST', check_csrf=True,
             permission='has_role:grader')
def grader_new_deadline_extension(request):
    lab = Lab.get_or_404(int(request.POST.getone('lab_id')))

    if lab.individual:
        submitter = Student.get_or_404(request.POST.getone('student_id'))
    else:
        submitter = Group.get_or_404(int(request.POST.getone('group_id')))

    try:
        first_deadline = datetime.strptime(request.POST.getone('first_deadline'), '%Y-%m-%d %H:%M')
    except ValueError, e:
        first_deadline = None

    try:
        final_deadline = datetime.strptime(request.POST.getone('final_deadline'), '%Y-%m-%d %H:%M')
    except ValueError, e:
        final_deadline = None

    lab.extend_deadline(submitter, first_deadline, final_deadline)

    return HTTPFound(request.route_path('grader_deadline_extensions'))


@view_config(route_name='grader_update_deadline_extension',
             request_method='POST', check_csrf=True,
             permission='has_role:grader')
def grader_update_deadline_extension(request):
    lab = Lab.get_or_404(int(request.POST.getone('lab_id')))

    if lab.individual:
        submitter = Student.get_or_404(request.POST.getone('student_id'))
    else:
        submitter = Group.get_or_404(int(request.POST.getone('group_id')))

    if request.POST.getone('action') == 'update':
        try:
            first_deadline = datetime.strptime(request.POST.getone('first_deadline'), '%Y-%m-%d %H:%M')
        except ValueError, e:
            first_deadline = None

        try:
            final_deadline = datetime.strptime(request.POST.getone('final_deadline'), '%Y-%m-%d %H:%M')
        except ValueError, e:
            final_deadline = None

        lab.extend_deadline(submitter, first_deadline, final_deadline)
    elif request.POST.getone('action') == 'delete':
        lab.remove_deadline_extension(submitter)

    return HTTPFound(request.route_path('grader_deadline_extensions'))


@view_config(route_name='grader_group_size_exceptions',
             request_method='GET',
             renderer='fire:templates/grader_groupsizes.html',
             permission='has_role:grader')
def grader_group_size_exceptions(request):
    return dict(labs=Lab.get_active(), groups=Group.get_all(),
                deadline_extensions=len(GroupDeadlineExtension.get_all()) + len(IndividualDeadlineExtension.get_all()),
                groupsize_exceptions=len(GroupSizeException.get_all()))


@view_config(route_name='grader_update_group_size_exception',
             request_method='POST', check_csrf=True,
             renderer='json',
             permission='has_role:grader')
def grader_update_group_size_exception(request):
    lab = Lab.get_or_404(int(request.POST.getone('lab_id')))
    group = Group.get_or_404(int(request.POST.getone('group_id')))

    added = 'add' in request.POST

    if added:
        lab.add_group_size_exception(group)
    else:
        lab.remove_group_size_exception(group)

    return dict(added=added)


@view_config(route_name='graph_submissions',
             request_method='GET',
             renderer='json',
             permission='has_role:grader')
def graph_submissions(request):
    """
    This view returns a json list of the number of submission per day for the
    last 42 days. It is used to build the github-style bar chart in the lab
    overview page.

    Note that we need to write a sql query manually here because
    sqlalchemy doesn't let us use the DATE function of sqlite.

    We do two query, one for the number of submission assigned to the
    current grader and one for the total number.
    """

    conn = DBSession.get_bind()

    lab_id = int(request.matchdict['lab'])
    grader_id = request.user.email
    today = date.today()

    data = dict(all=[], me=[])

    query = text("""
      SELECT DATE(date), COUNT(*)
      FROM Submissions
      WHERE DATE(date) >= DATE(julianday(date('now'))-41)
      AND lab_id = :lab_id
      GROUP BY DATE(date)
    """)
    dbdata = dict(list(conn.execute(query, lab_id=lab_id)))
    for i in range(0, 42):
        d = today - timedelta(days=i)
        data['all'].append(dbdata.get(str(d), 0))

    query = text("""
      SELECT DATE(S.date), COUNT(*)
      FROM Submissions AS S
        JOIN grader_assignments ON S.id == grader_assignments.submission_id
      WHERE DATE(S.date) >= DATE(julianday(date('now'))-41)
      AND lab_id = :lab_id
      AND grader_id = :grader_id
      GROUP BY DATE(S.date)
    """)
    dbdata = dict(list(conn.execute(query, lab_id=lab_id, grader_id=grader_id)))
    for i in range(0, 42):
        d = today - timedelta(days=i)
        data['me'].append(dbdata.get(str(d), 0))

    return data


@view_config(route_name='grader_notifications',
             renderer='fire:templates/grader_notifications.html',
             permission='has_role:grader')
def grader_notifications(request):
    request.flash_fire(msg="Not available at this time.")
    return dict(form=make_form(GraderNotificationsSchema()))
