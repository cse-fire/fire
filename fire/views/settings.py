import colander
from pyramid.view import view_config, view_defaults
from deform import Form
import deform.widget
from deform import ValidationFailure
from pyramid.httpexceptions import HTTPFound

from ..models import DBSession, Lab, GroupLab, AllowedFileExtension, CheckHook, Settings, \
                     GroupDeadlineExtension, IndividualDeadlineExtension, GroupSizeException
from ..forms import genshi_deform_renderer, DateTime, DateTimeInput, make_form
from ..schemata import LabNode, URLNode


# ~~~ Validation Schema ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class Extensions(colander.SequenceSchema):
    def normalize_file_extension(ext):
        ext = ext.strip()
        return ext if ext.startswith('.') else '.' + ext

    extension = colander.SchemaNode(
        colander.String(),
        title="Extension",
        preparer=normalize_file_extension)

    def deserialize(self, cstruct):
        exts = super(self.__class__, self).deserialize(cstruct)
        return [AllowedFileExtension(extension=e) for e in exts]


class IndividualLabSchema(colander.MappingSchema):
    title = colander.SchemaNode(colander.String(), title='Title')
    slug = colander.SchemaNode(colander.String(), title='Slug',
                               description="Short name of the lab displayed in the course roaster")
    allow_physical_submissions = colander.SchemaNode(colander.Boolean(), title='Allow physical submissions')
    autoassign_grader = colander.SchemaNode(colander.Boolean(), default=True,
                                            title='Graders are assigned automatically')
    predecessor = LabNode(
        title='Predecessor', missing=None,
        description="Use the same grader as this lab, if any. ")
    first_deadline = colander.SchemaNode(DateTime(),
                                         title='First deadline',
                                         widget=DateTimeInput(),
                                         missing=None,
        description="""This is the deadline for the first submission. I.e. if
        a student has not submitted by this time, they won't be able to submit
        to this lab anymore (unless granted a deadline extension).
        On the other hand, a student who submitted at least once before this
        deadline will be allowed to re-submit until the final deadline.
        Leave empty if you don't want a deadline for the first submission.""")
    final_deadline = colander.SchemaNode(DateTime(),
                                         title='Final deadline',
                                         widget=DateTimeInput(),
                                         missing=None,
        description="""No submission or re-submission will be allowed after
        this time. Leaving it empty allows student to re-submit forever.""")

    allowed_file_extensions = Extensions(
        title="Allowed file extensions",
        widget=deform.widget.TextInputCSVWidget(),
        missing=[],
        description="""A comma separated list of file extension allowed for this lab,
                       e.g.: ".pdf, .odt, .rtf". Leave empty to allow all extensions.""")


class GroupLabSchema(IndividualLabSchema):
    min_members = colander.SchemaNode(colander.Int(), title='Min members',
                                      validator=colander.Range(min=1))
    max_members = colander.SchemaNode(colander.Int(), title='Max members')


class LabCheckSchema(colander.MappingSchema):
    """ This is the form used to add automatic lab checks. """
    name = colander.SchemaNode(colander.String(), title="Check name")
    url_template = colander.SchemaNode(colander.String(), title="URL template", validator=colander.url)
    auto_accept = colander.SchemaNode(colander.Boolean(), title="Auto-accept on success")
    auto_reject = colander.SchemaNode(colander.Boolean(), title="Auto-reject on failure")


# ~~~ Creating new labs ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@view_defaults(route_name='new_lab', permission='has_role:grader',
               renderer='fire:templates/new_lab.html')
class NewLabView(object):
    """
    Views that allow admin to add a lab.
    """

    def __init__(self, request):
        self.request = request
        labs = Lab.get_active()
        self.individual = 'individual' in request.GET
        schema = IndividualLabSchema() if self.individual else GroupLabSchema()
        self.form = make_form(schema.bind(labs=labs),
                              buttons=('Add',),
                              css_class='form-horizontal')

    @view_config(request_method="GET", renderer='fire:templates/new_lab.html')
    def get(self):
        return dict(form=self.form, individual=self.individual)

    @view_config(request_method='POST', check_csrf=True)
    def post(self):
        try:
            appstruct = self.form.validate(self.request.POST.items())
            if self.individual:
                lab = Lab(**appstruct)
            else:
                lab = GroupLab(**appstruct)
            DBSession.add(lab)
            return HTTPFound(self.request.route_path('grader_main'))
        except ValidationFailure, e:
            return dict(form=e, individual=self.individual)


@view_defaults(route_name='lab_settings',
               renderer='fire:templates/lab_settings.html',
               permission='has_role:grader')
class LabSettingsView(object):
    def __init__(self, request):
        self.request = request
        self.lab = Lab.get_or_404(int(request.matchdict['lab']))
        labs = [l for l in Lab.get_active() if l.id != self.lab.id]
        schema = IndividualLabSchema() if self.lab.individual else GroupLabSchema()
        self.form = make_form(schema.bind(labs=labs),
                              buttons=('submit',),
                              css_class='form-horizontal')

    @view_config(request_method='GET')
    def get(self):
        appstruct = dict(
            title=self.lab.title,
            slug=self.lab.slug,
            first_deadline=self.lab.first_deadline,
            final_deadline=self.lab.final_deadline,
            autoassign_grader=self.lab.autoassign_grader,
            predecessor=self.lab.predecessor or colander.null,
            allow_physical_submissions=self.lab.allow_physical_submissions,
            allowed_file_extensions=self.lab.allowed_file_extensions,
            min_members=self.lab.min_members,
            max_members=self.lab.max_members)
        return dict(form=self.form.render(appstruct), lab=self.lab)

    @view_config(request_method='POST', check_csrf=True)
    def post(self):
        assert 'submit' in self.request.POST
        controls = self.request.POST.items()
        try:
            appstruct = self.form.validate(controls)
            for key, value in appstruct.iteritems():
                setattr(self.lab, key, value)
        except ValidationFailure, e:
            return {'form': e.render(),
                    'values': False,
                    'title': "error",
                    'lab': self.lab}
        self.request.flash_fire(msg="Lab updated.")
        return HTTPFound(
            location=self.request.route_path('grader_lab', lab=self.lab.id))


@view_defaults(route_name='lab_delete',
               permission='has_role:grader')
class LabDeleteView(object):
    def __init__(self, request):
        self.request = request
        self.lab = Lab.get_or_404(int(request.matchdict['lab']))

    @view_config(request_method='POST', check_csrf=True)
    def post(self):
        assert self.lab.can_be_deleted
        DBSession.delete(self.lab)
        self.request.flash_fire(msg='Lab "%s" has been deleted.' % self.lab.title)
        return HTTPFound(location=self.request.route_path('grader_main'))


@view_defaults(route_name="lab_checks",
               renderer='fire:templates/lab_checks.html',
               permission='has_role:grader')
class LabChecksView(object):
    def __init__(self, request):
        self.lab = Lab.get_or_404(int(request.matchdict['lab']))
        self.request = request
        self.form = Form(
            LabCheckSchema(), buttons=('add',),
            renderer=genshi_deform_renderer,
            csrf_token=request.session.get_csrf_token()
        )

    @view_config(request_method='GET')
    def get(self):
        return dict(lab=self.lab)

    @view_config(request_method='POST', request_param='delete')
    def delete(self):
        check = CheckHook.get_or_404((self.request.POST['delete'], self.lab.id))
        DBSession.delete(check)
        return HTTPFound(
            location=self.request.route_path('lab_checks', lab=self.lab.id))

    @view_config(request_method='POST', check_csrf=True)
    def post(self):
        controls = self.request.POST.items()
        try:
            appstruct = self.form.validate(controls)
            self.lab.checks.append(CheckHook(**appstruct))
            return HTTPFound(
                location=self.request.route_path('lab_checks', lab=self.lab.id))
        except ValidationFailure, e:
            return dict(form=e.render(), lab=self.lab)


class CourseSettingsSchema(colander.MappingSchema):
    course_name = colander.SchemaNode(colander.String(), title='Name')
    course_url = URLNode(missing='', title='Web page URL')
    disable_registration = colander.SchemaNode(colander.Boolean(), title='Disable registration')


@view_defaults(route_name='settings',
               renderer='fire:templates/course_settings.html',
               permission='has_role:grader')
class CourseSettings(object):
    def __init__(self, request):
        self.request = request
        schema = CourseSettingsSchema()
        self.form = make_form(schema,
                              buttons=('submit',),
                              css_class='form-horizontal')

    @view_config(request_method='GET')
    def get(self):
        return dict(deadline_extensions=len(GroupDeadlineExtension.get_all()) + len(IndividualDeadlineExtension.get_all()),
                    groupsize_exceptions=len(GroupSizeException.get_all()),
                    form=self.form.render(Settings.get_all()))

    @view_config(request_method='POST', check_csrf=True)
    def post(self):
        controls = self.request.POST.items()
        try:
            appstruct = self.form.validate(controls)
            for key, value in appstruct.iteritems():
                if Settings.exists(key):
                    Settings.set(key, value)
        except ValidationFailure, e:
            return {'form': e.render(),
                    'values': False,
                    'title': "error"}
        self.request.flash_fire(msg="Settings updated.")
        return HTTPFound(
            location=self.request.route_path('settings'))
