import logging

from pyramid.view import view_config
from pyramid.httpexceptions import (
    HTTPNoContent, HTTPNotFound, HTTPGone, HTTPBadRequest)

from fire.models import CheckResult, Markdown


LOG = logging.getLogger(__name__)


@view_config(
    route_name='check_feedback',
    permission='__no_permission_required__')
def check_feedback(request):
    """ Record feedback from automated checks.  Feedback should be either
    'pass' or 'fail' otherwise, consider it an error.  If feedback is already
    been set for this token, we return HTTP error 410 'Gone'.
    """
    job = CheckResult.get(request.matchdict['token'])
    if job is None:
        return HTTPNotFound("Invalid token")
    if job.status != CheckResult.PENDING:
        LOG.debug("Job status is %s. Not overwriting", job.status)
        return HTTPGone("This job has already reported feedback")
    if 'status' not in request.params:
        return HTTPBadRequest('Missing parameter "status"')
    status = request.params['status']
    if status not in ('pass', 'fail'):
        return HTTPBadRequest('Invalid status')
    job.status = job.PASS if status == 'pass' else job.FAIL
    job.details_url = Markdown(request.body.decode('utf-8'))
    return HTTPNoContent(headers={"msg": str(request.matchdict)})
