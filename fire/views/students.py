from pyramid.view import view_config, view_defaults
from pyramid.httpexceptions import HTTPNotFound, HTTPFound, HTTPBadRequest

from ..check import check_extension
from ..events import NewSubmission
from ..models import Grader, Lab, Submission, Group, DBSession, FileCap, Markdown
from .. import filestore, templating, settings
from ..errors import UserException, InternalFireError

import logging

log = logging.getLogger(__name__)


@view_config(route_name='student_main',
             renderer='fire:templates/student_main.html',
             permission='has_role:student')
def student_main(request):
    student = request.user
    labs = Lab.get_active()
    student_info = []
    for lab in labs:
        deciding_submission = lab.deciding_submission_for_submitter(student)
        effective_deadline = student.effective_deadline(lab)
        # Note that this is not the same set of constrains as in `SubmitterMixin.can_submit`. Specifically,
        # we do not check the group size and double submissions. The reason is because we would rather display
        # a verbose error message on the submission page.
        can_submit = student.can_submit(lab, ignore_group_constraints=True)
        info = dict(deciding_submission=deciding_submission, can_submit=can_submit, effective_deadline=effective_deadline)
        student_info.append((lab, info))
    return dict(student_info=student_info)


@view_config(route_name='student_lab',
             renderer='fire:templates/student_lab.html',
             permission='has_role:student')
def student_lab(request):
    lab = Lab.get_or_404(int(request.matchdict['lab']))

    if lab.is_suspended:
        return HTTPFound(location=request.route_path('student_main'))

    student = request.user

    submissions = sorted(student.submissions_for_lab(lab), key=lambda s: s.date, reverse=True)

    if lab.individual:
        group = None
        submitter = student
    else:
        group = student.group
        submitter = group

    active_member = group and (student in group.active_members)
    valid_submitters = submitter.valid_members(lab) if submitter else []
    deadline = lab.get_deadlines(submitter, full_info=True)
    draftdir = filestore.DraftDir(settings.get_static('data_dir'), lab, submitter) if submitter else None

    submitter_history = [(l, l.deciding_submission_for_submitter(submitter)) for l in Lab.get_active()]

    return dict(
        lab=lab,
        deadline=deadline,
        submissions=submissions,
        submitter_history=submitter_history,
        group=group,
        draftdir=draftdir,
        valid_submitters=valid_submitters,
        active_member=active_member,
        submitter=submitter
    )


@view_config(
    route_name='student_lab',
    request_method='POST',
    request_param='withdraw',
    check_csrf=True,
    permission='has_role:student')
def student_withdraw_submission(request):
    """
    View that allow a student to withdraw a submission.
    Note that the view config makes sure that we only call this method
    if on a POST request where there is a parameter called 'withdraw'.
    """
    submission = Submission.get(int(request.params['withdraw']))
    try:
        if not submission.can_be_withdrawn():
            raise UserException("You are not allowed to withdraw this submission")
        submission.withdraw()
        request.flash_fire(msg='Submission withdrawn')
    except Exception, e:
        request.flash_fire(type='error', msg=str(e))
    return HTTPFound(location=request.route_path('student_lab', lab=submission.lab_id))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# Submission Drafts
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
@view_defaults(permission='has_role:student')
class StudentDraftView(object):
    """ View allowing student to see and manipulate draft submissionsfor a lab
    """
    def __init__(self, request):
        self.request = request
        self.lab = Lab.get_or_404(int(request.matchdict['lab']))
        student = request.user
        if self.lab.individual:
            self.submitter = student
        else:
            if not student.group:
                raise UserException("You must create or join a group before submitting to this lab.")
            self.submitter = student.group
        self.draftdir = filestore.DraftDir(settings.get_static('data_dir'), self.lab, self.submitter)

    @view_config(route_name='student_upload',
                 request_method='POST', renderer='json')
    def upload(self):
        log.debug("Starting student upload")
        # TODO check file sizes, name restrictions etc.
        if not self.submitter.can_submit(self.lab):
            return dict(
                error="Error: You are not allowed to submit to this lab")

        file_name = self.request.POST['files[]'].filename
        file_data = self.request.POST['files[]'].file
        if not check_extension(file_name, self.lab):
            return dict(error="Invalid file extension")
        try:
            info = self.draftdir.store_file(file_name, file_data)
            return dict(
                success='ok',
                tablerow=unicode(templating.filelisting_row(
                    info, browsable=False, draft=True)))
        except UserException, e:
            log.exception(e)
            return dict(error="Error: " + e.message)
        except InternalFireError, e:
            log.exception(e)
            return dict(error="File upload failed")

    @view_config(route_name='student_delete_file',
                 request_method='POST', renderer='json')
    def delete_file(self):
        filename = self.request.body
        try:
            self.draftdir.delete_file(filename)
            return dict(success="ok")
        except Exception, e:
            log.exception(e)
            return dict(error="The file could not be deleted")

    @view_config(route_name='student_download_draft_file')
    def student_download_draft_file(self):
        filename = self.request.matchdict['filename']
        files = {f.name:f for f in self.draftdir.ls()['files']}
        try:
            full_path = files[filename].path
        except KeyError:
            return HTTPNotFound()

        cap = FileCap.issue_single(full_path, filename)

        file_cap = self.request.route_path(
            'get_filecap', filecap_id=cap.id, filename=cap.filename)
        cap_url = settings.get(self.request, 'url_capserver') + file_cap

        if self.request.params.getall("dl"):
            cap_url += "?dl=1"

        return HTTPFound(cap_url)

    @view_config(route_name='student_submit', request_method='POST')
    def student_submit(self):
        if self.lab.is_suspended:
            return HTTPFound(location=self.request.route_path('student_main'))

        if not self.submitter.can_submit(self.lab):
            return HTTPBadRequest()

        physical_submission = self.request.POST.getone('physical') == 'yes'

        if 'comment' in self.request.POST:
            comment = Markdown(self.request.POST['comment'])
        else:
            comment = None

        if self.submitter.must_choose_grader(self.lab):
            if 'grader' in self.request.POST:
                grader_id = self.request.POST['grader']
                if grader_id in (rg.id for rg in self.lab.responsible_graders):
                    grader = Grader(grader_id)
                else:
                    return HTTPBadRequest()
            else:
                return HTTPBadRequest()
        else:
            grader = self.lab.choose_grader(self.submitter)

        submission = Submission(self.lab, self.submitter, comment, physical_submission)
        submission.assign_grader(grader)

        try:
            filestore.materialise(self.draftdir, submission)
            if comment:
                filestore.store_comment(submission, comment)
        except OSError, e:
            log.exception(e)
            msg = "Submission failed due to unknown error. Please report this incident."
            self.request.flash_fire(type='error', msg=msg)

        self.request.registry.notify(NewSubmission(submission, self.request))
        return HTTPFound(location=self.request.route_path('student_lab', lab=self.lab.id))


@view_config(route_name='student_newgroup',
             request_method='POST', check_csrf=True,
             permission='has_role:student')
def student_newgroup(request):
    if request.user.group:
        request.flash_fire(type='error', msg='Leave the current group before creating a new one.')
    else:
        join_code = request.POST.getone('joincode')
        if join_code:
            group = Group.get_where(Group.joincode == join_code)
            if not group:
                request.flash_fire(type='error', msg="The join code you entered is invalid.")
            else:
                assert len(group) == 1  # join-codes need to be unique
                group[0].add_user(request.user)
                request.flash_fire(type='success', msg=("You are now a member of group %d" % group[0].id))
        else:
            group = Group.newGroupForStudent(request.user)
            DBSession().add(group)
            request.flash_fire(type='success', msg="Successfully created a new group.")

    return HTTPFound(location=request.route_path('student_main'))


@view_config(route_name='student_leavegroup',
             request_method='POST', check_csrf=True,
             permission='has_role:student')
def student_leavegroup(request):
    if request.user.group:
        request.user.group.remove_user(request.user)
    return HTTPFound(location=request.route_path('student_main'))
