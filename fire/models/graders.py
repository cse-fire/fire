from sqlalchemy.schema import Column, ForeignKey, CheckConstraint
from sqlalchemy.types import Integer, Text
from sqlalchemy.orm import relationship

from pyramid.security import Allow, ALL_PERMISSIONS

from .meta import Base
from .users import User


class Grader(User):
    __tablename__ = 'graders'
    __mapper_args__ = {'polymorphic_identity': 'grader'}
    email = Column(Text, ForeignKey('users.email'), primary_key=True)

    __acl__ = [(Allow, 'role:admin', ALL_PERMISSIONS)]


class GraderWorkload(Base):
    """ Represent the fact that a grader is assigned to a lab. The weight
    is used when a new submission comes in to randomly assign a grader to
    that submission, it allow precise control of the submission distribution.
    """
    __tablename__ = 'grader_workload'

    lab_id = Column(Integer, ForeignKey('labs.id'), primary_key=True)
    grader_id = Column(Text, ForeignKey('graders.email'), nullable=False, primary_key=True)
    weight = Column(Integer, CheckConstraint('weight > 0'), nullable=False, default=1)

    grader = relationship(Grader)

    def __init__(self, lab, grader, weight=1):
        self.lab_id = lab.id
        self.grader_id = grader.email
        self.weight = weight
