import logging

from sqlalchemy.schema import Column, ForeignKey
from sqlalchemy.types import Text, DateTime, String, Integer
from sqlalchemy.orm import relationship, object_session

from pyramid.security import Allow, ALL_PERMISSIONS

from datetime import datetime, timedelta
import random

from ..errors import InternalFireError

from .meta import Base
from .users import User
from .reviews import Review


class SubmitterMixin(object):
    """A mixin for Users and Groups. Includes common functionality for
    submitters."""

    # TODO: perhaps these are better kept in the Lab class. Refactor?

    def submissions_for_lab(self, lab):
        """Returns all submissions for a given lab for this user or group."""
        return [sub for sub in self.submissions if sub.lab_id == lab.id]

    def pending_submission(self, lab):
        """Returns the pending submission for a lab, if one exists, None otherwise."""
        subs = [sub for sub in self.submissions_for_lab(lab) if sub.is_active and not sub.review]
        if subs:
            assert len(subs) == 1  # Should be maintained always
            return subs[0]
        else:
            return None

    def accepted_submissions(self, lab):
        return [sub.review and sub.review.is_accepted for sub in self.submissions_for_lab(lab)]

    def most_recent_submitted_lab(self):
        """Returns the lab of the most recent submission. Otherwise, returns None"""
        subs = [sub for sub in self.submissions]
        if subs:
            subs.sort(key = lambda s: s.date, reverse=True)
            return subs[0].lab
        else:
            return None

    def enough_members(self, lab):
        """Checks that the submitter has enough members to submit to a given lab.
           -1 : not enough members
            0 : acceptable number of members
            1 : too many members
        """
        if isinstance(self, Group) and lab.get_group_size_exception(self):
            return 0

        members = len(self.active_members)

        if members < lab.min_members:
            return -1

        if members > lab.max_members:
            return 1

        return 0

    def effective_deadline(self, lab):
        """Calculates the effective deadline that the submitter has in a given lab."""
        first_deadline, final_deadline = lab.get_deadlines(self)

        has_reviewed_submissions = any(submission.review is not None for submission in self.submissions_for_lab(lab))

        if has_reviewed_submissions:
            return final_deadline
        else:
            return first_deadline or final_deadline

    def in_time(self, lab):
        """ Checks that the submitter is within the deadline for submission to
        a given lab.  We add a one minute grace periode after the deadline
        during which the function still returns true to avoid problems due to
        slow networks or a sligtly off server clock.
        """
        deadline = self.effective_deadline(lab)
        if not deadline:
            return True

        return datetime.now() - timedelta(minutes=1) <= deadline

    def valid_members(self, lab):
        return [s for s in self.active_members if not s.has_double_submissions(lab)]

    def must_choose_grader(self, lab):
        # Cannot choose a grader if cannot submit at all, duh
        if not self.can_submit(lab):
            return False

        # Cannot choose a grader if already in the middle of resubmissions
        if lab.last_grader(self):
            return False

        # Predecessors overrule manual choice of graders
        if lab.predecessor:
            return False

        # The lab must allow for manual grader assignment
        if lab.autoassign_grader:
            return False

        return True

    def can_submit(self, lab, ignore_group_constraints=False):
        """Checks that all criteria for a submission were met"""
        def log_reason(r):
            logging.getLogger(__name__).debug("%s cannot submit %s because %s", self, lab, r)
        # Cannot submit if the lab is suspended
        if lab.is_suspended:
            log_reason("lab is suspended")
            return False

        # Cannot submit if there is an existing pending submission
        if self.pending_submission(lab):
            log_reason("existing pending submission")
            return False

        # Cannot submit if the deadline has passed
        if not self.in_time(lab):
            log_reason("deadline has passed")
            return False

        # Cannot submit if already has an accepted submission. This behaviour is likely
        # to be tuned if we allow resubmission after acceptance.
        if any(self.accepted_submissions(lab)):
            log_reason("existing accepted submission")
            return False

        if not ignore_group_constraints:
            # Cannot submit if the group size is not right
            if self.enough_members(lab) != 0:
                log_reason("group size")
                return False

            # Cannot submit if any of the group member have submissions as part of other groups
            # TODO: It should be possible for a student to submit even if one of a groupmates
            # has a double submission. In such case the offending group mate should be excluded from
            # the list of submitters
            # if any(submitter.has_double_submissions(lab) for submitter in self.active_members):
            #     log_reason("double submissions")
            #     return False

        return True


class Student(User, SubmitterMixin):
    __tablename__ = 'students'
    __mapper_args__ = {'polymorphic_identity': 'student'}
    email = Column(Text, ForeignKey('users.email'), primary_key=True)
    id_number = Column(Text, unique=True)                        #: student id number
    course = Column(String(10), ForeignKey('coursecodes.code', ondelete='restrict'))
    group_id = Column(Integer, ForeignKey('groups.id'))
    signup = Column(DateTime, default=datetime.now)  #: date signed up

    __acl__ = [(Allow, 'role:grader', 'view'),
               (Allow, 'role:grader', 'edit'),
               (Allow, 'role:grader', 'impersonate'),
               (Allow, 'role:admin', ALL_PERMISSIONS)]

    @property
    def active_members(self):
        return [self]

    def has_double_submissions(self, lab):
        if lab.individual:
            return False
        else:
            return any(s.is_pending and s.group_id != self.group_id for s in self.submissions_for_lab(lab))


_joincodeletters = 'abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ'


def mkjoincode():
    groups = Group.get_all()
    existing_codes = set(g.joincode for g in groups)
    for i in xrange(512):
        code = ''.join(random.choice(_joincodeletters) for i in xrange(6))
        if code not in existing_codes:
            return code
    # Probability of reaching here: 1/(Infinity - 1)
    raise InternalFireError("Failed to generate a joincode.")


class Group(Base, SubmitterMixin):
    __tablename__ = 'groups'

    id = Column(Integer, primary_key=True)
    created = Column(DateTime, default=datetime.now)
    joincode = Column(Text, nullable=False, default=mkjoincode)

    members = relationship("Student", backref="group")

    @property
    def name(self):
        return "Group %s" % self.id if self.id is not None else "?"

    @classmethod
    def newGroupForStudent(cls, owner):
        """ A factory method to create a new group with the given student """
        assert isinstance(owner, Student)
        group = cls()
        group.members.append(owner)
        return group

    @property
    def active_members(self):
        return [u for u in self.members]

    def add_user(self, user):
        if user not in self.active_members:
            self.members.append(user)

    def remove_user(self, user):
        if user not in self.active_members:
            raise ValueError("Group.remove_user called with a non-member.")
        self.members.remove(user)

    @property
    def slug(self):
        return 'group%02d' % self.id

    def __repr__(self):
        return "<%s>" % self.name

    def __unicode__(self):
        return u"%s" % self.name
