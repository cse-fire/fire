from sqlalchemy.schema import Column, ForeignKey
from sqlalchemy.types import Text, DateTime, String
from sqlalchemy.ext.hybrid import hybrid_property

from pyramid.security import Allow, ALL_PERMISSIONS

from datetime import datetime

from ..security import hash_pwd, verify_pwd, random_hex_string

from .meta import Base, DBSession, DeclEnum


class User(Base):
    """A user in the system (any kind; students, teachers, admins)
    """
    __tablename__ = 'users'

    class Roles(DeclEnum):
        student = 'student', 'Student'
        grader = 'grader', 'Grader'
        admin = 'admin', 'Course Admin'

    email = Column(Text, primary_key=True)
    _pw_hash = Column('pw_hash', Text)
    first_name = Column(Text)
    last_name = Column(Text)
    last_login = Column(DateTime)  # date of last login
    role = Column(String(50), nullable=False)

    __mapper_args__ = {'polymorphic_on': role}

    @property
    def id(self):
        """ ID property, created for backward compatibility"""
        return self.email

    @property
    def slug(self):
        return self.email

    def __init__(self, email, **kwargs):
        super(User, self).__init__(**kwargs)
        self.email = email

    @classmethod
    def create(cls, *args, **kargs):
        dbsession = kargs.pop('dbsession', DBSession)
        u = cls(*args, **kargs)
        dbsession.add(u)

    @property
    def __acl__(self):
        return [(Allow, 'role:admin', ALL_PERMISSIONS)]

    @hybrid_property
    def password(self):
        """Always an empty string, only used for setting a password."""
        # Note: As a convention, do not handle _pw_hash outside this class.
        # It contains a salted hash, so leaking it opens up the possibility
        # of rainbow attacks.
        return ""

    @password.setter
    def password(self, value):
        """Updates the stored hashed password, actual value is not stored."""
        self._pw_hash = hash_pwd(value)

    @classmethod
    def check_password(cls, userid, pwd, dbsession=None):
        """Returns true if ``pwd`` matches the stored hash of the users password.
        Returns false if the user is not verified.
        """
        u = cls.get(userid, dbsession=dbsession)
        if u is not None:
            return verify_pwd(pwd, u._pw_hash)
        return False

    @property
    def name(self):
        if self.first_name and self.last_name:
            return "%s %s" % (self.first_name, self.last_name)
        elif self.first_name:
            return self.first_name
        elif self.last_name:
            return self.last_name
        else:
            return None

    @property
    def safe_info(self):
        """Returns a dictionary of information about the user that is safe to display."""
        info = dict()
        for prop in ['id', 'email', 'name', 'id_number', 'course', 'role']:
            info[prop] = getattr(self, prop, None)
        return info

    def __repr__(self):
        return "<User %s>" % self.email

    def __unicode__(self):
        return u"%s <%s>" % (self.name, self.email)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.email == other.email

        # We would like to sometimes compare User objects with user's safe_info
        # For that reason, equality is overloaded to support dicts
        if isinstance(other, dict):
            return self.email == other['email']

        return False

    def __ne__(self, other):
        return not self.__eq__(other)


class EmailVerification(Base):
    """ Allow to verify an email when users register"""
    __tablename__ = 'emailverifications'
    email = Column(Text)
    token = Column(Text, primary_key=True, default=lambda: random_hex_string(16))
    cdate = Column(DateTime, default=datetime.now)

    @staticmethod
    def create(dbsession=DBSession, **kwargs):
        verif = EmailVerification(**kwargs)
        dbsession.add(verif)
        dbsession.flush()
        return verif

    @classmethod
    def check(cls, token, dbsession=DBSession):
        """ Checks a token: returns the email address for a valid token
        or raise an exception """
        verif = dbsession.query(cls).get(token)
        if verif is None:
            raise ValueError('Invalid verification token')
        else:
            return verif.email

    @classmethod
    def get_unverified(cls, dbsession=None):
        """ Returns rows that don't correspond to a user account """
        dbsession = dbsession or DBSession()
        verified = dbsession.query(User.email).subquery()
        return dbsession.query(cls).filter(~cls.email.in_(verified)).all()


class PasswordReset(Base):
    """Allows to reset users password"""
    __tablename__ = 'passwordresets'
    email = Column(Text, ForeignKey('users.email'))
    token = Column(Text, primary_key=True, default=lambda: random_hex_string(16))
    cdate = Column(DateTime, default=datetime.now)

    @staticmethod
    def create(dbsession=DBSession, **kwargs):
        reset = PasswordReset(**kwargs)
        dbsession.add(reset)
        dbsession.flush()
        return reset

    @classmethod
    def check(cls, token):
        """ Checks a token: returns the email address for a valid token
        or raise an exception """
        reset = DBSession.query(cls).get(token)
        if reset is None:
            raise ValueError('Invalid password reset token.')
        else:
            return reset.email
