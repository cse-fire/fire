from .meta import Base, DBSession

from sqlalchemy.schema import Column, CheckConstraint
from sqlalchemy.types import Text, PickleType, String

from ..errors import InternalFireError


class Settings(Base):
    """Course wide settings.
    """
    __tablename__ = 'settings'

    id = Column(Text, primary_key=True)
    value = Column(PickleType)

    _defaults = dict(course_name='Default course',
                     course_url='',
                     disable_registration=False)

    _cache = dict()

    @classmethod
    def keys(cls):
        return cls._defaults.keys()

    @classmethod
    def exists(cls, key):
        return key in cls.keys()

    # This class deviates from base, as other code should not
    # deal directly with instances
    @classmethod
    def get(cls, key):
        if not cls.exists(key):
            raise InternalFireError("Invalid setting name %s" % key)
        if key in cls._cache:
            return cls._cache[key]
        obj = DBSession().query(cls).get(key)
        if obj is None:
            return cls._defaults[key]
        else:
            cls._cache[key] = obj.value
            return obj.value

    @classmethod
    def get_all(cls):
        return {key: cls.get(key) for key in cls.keys()}

    @classmethod
    def set(cls, key, value):
        if not cls.exists(key):
            raise InternalFireError("Invalid setting name %s" % key)
        cls._cache.pop(key, None)
        obj = DBSession().query(cls).get(key)
        if obj is None:
            obj = cls(id=key)
            DBSession().add(obj)
        obj.value = value


class CourseCode(Base):
    __tablename__ = 'coursecodes'
    code = Column(String(10), CheckConstraint("code <> ''"), primary_key=True)
