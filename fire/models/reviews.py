from sqlalchemy.schema import Column, ForeignKey
from sqlalchemy.types import Integer, Text, DateTime
from sqlalchemy.orm import relationship

from datetime import datetime

from .meta import Base, DeclEnum
from .graders import Grader
from .instrumental import MarkdownType


class Review(Base):
    __tablename__ = 'reviews'

    class Decision(DeclEnum):
        accepted = 'accepted', 'Accepted'
        rejected = 'rejected', 'Rejected'

    submission_id = Column(Integer, ForeignKey('submissions.id'),
                           primary_key=True)
    date = Column(DateTime, primary_key=True)
    grader_id = Column(Text, ForeignKey('graders.email'))
    decision = Column(Decision.db_type(), nullable=False)
    grade = Column(Text)
    note = Column(Text)
    review = Column(MarkdownType)

    grader = relationship(Grader, backref='reviews',
                          primaryjoin=grader_id == Grader.email)

    def __init__(self, submission, accepted, grader, grade, note, review):
        self.submission_id = submission.id
        self.decision = (
            Review.Decision.accepted if accepted else Review.Decision.rejected)
        self.grader_id = grader
        self.date = datetime.now()
        self.note = note
        self.grade = grade
        self.review = review

    @property
    def is_accepted(self):
        return self.decision == Review.Decision.accepted


class ReviewDraft(Base):
    __tablename__ = 'review_drafts'

    submission_id = Column(Integer, ForeignKey('submissions.id'), primary_key=True)
    grader_id = Column(Text, ForeignKey('graders.email'))
    date = Column(DateTime, default=datetime.now, nullable=False)

    grade = Column(Text)
    note = Column(Text)
    review = Column(MarkdownType)

    def __init__(self, submission, grader, grade, note, review):
        self.submission_id = submission.id
        self.grader_id = grader.id
        self.grade = grade
        self.note = note
        self.review = review

    @classmethod
    def fetch(cls, submission):
        return cls.get((submission.id,))
