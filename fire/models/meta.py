from pyramid.httpexceptions import HTTPNotFound
from sqlalchemy import event
from sqlalchemy.types import SchemaType, TypeDecorator, Enum
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.engine import Engine
# from zope.sqlalchemy import ZopeTransactionExtension
from zope.sqlalchemy import register

import re


# ENABLE FOREIGN KEY CONSTRAINTS
# SQLite supports FOREIGN KEY syntax when emitting CREATE statements for tables,
# however by default these constraints have no effect on the operation of the
# table. Need to turn it on for each connection!
@event.listens_for(Engine, "connect")
def set_sqlite_pragma(dbapi_connection, connection_record):
    cursor = dbapi_connection.cursor()
    cursor.execute("PRAGMA foreign_keys=ON")
    cursor.close()


DBSession = scoped_session(sessionmaker(expire_on_commit=False))
register(DBSession)

def includeme(config):
    from sqlalchemy import engine_from_config
    engine = engine_from_config(config.registry.settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)


def model_resource(model, match=None, postparam=None):
    """Creates a resource factory, suitable for passing to Pyramid's
    add_route, that returns a model class (or 404 if not found).
    The id for the model class is taken from the URL routing matchdict
    if the match parameter is used, or from the POST data if the postaparm
    is given.

    Using such a factory makes Pyramid pass the model instance as the
    context parameter to the view, after consulting the instance's
    __acl__ property to see if the current user has permission to the object.
    """
    # We kind of rely on that SQLite doesn't care about types. A different db
    # would mean here we need to add some type conversion as well.
    if match:
        def model_resource_factory(request):
            return model.get_or_404(request.matchdict[match])
    elif postparam:
        def model_resource_factory(request):
            return model.get_or_404(request.POST.getone(postparam))
    else:
        raise ValueError("Must specify either a name from matchdict or request.POST to use as an id")
    return model_resource_factory


class Base(object):
    """A declarative base for SQLAlchemy models."""

    @classmethod
    def get(cls, key, dbsession=None):
        dbsession = dbsession or DBSession()
        return dbsession.query(cls).get(key)

    @classmethod
    def get_or_404(cls, key):
        obj = cls.get(key)
        if obj is None:
            raise HTTPNotFound("No such %s : %s" % (cls.__name__, str(key)))
        return obj

    @classmethod
    def get_where(cls, *args, **kwargs):
        return DBSession().query(cls).filter(*args, **kwargs).all()

    @classmethod
    def get_all(cls, *args, **kwargs):
        return DBSession().query(cls).order_by(*args, **kwargs).all()

    @classmethod
    def query(cls):
        return DBSession().query(cls)

Base = declarative_base(cls=Base)


# From Zzzeek's Enum recipe {{{
# http://techspot.zzzeek.org/2011/01/14/the-enum-recipe/

class EnumSymbol(object):
    """Define a fixed symbol tied to a parent class."""

    def __init__(self, cls_, name, value, description):
        self.cls_ = cls_
        self.name = name
        self.value = value
        self.description = description

    def __reduce__(self):
        """Allow unpickling to return the symbol
        linked to the DeclEnum class."""
        return getattr, (self.cls_, self.name)

    def __iter__(self):
        return iter([self.value, self.description])

    def __repr__(self):
        return "<%s>" % self.name


class EnumMeta(type):
    """Generate new DeclEnum classes."""

    def __init__(cls, classname, bases, dict_):
        cls._reg = reg = cls._reg.copy()
        for k, v in dict_.items():
            if isinstance(v, tuple):
                sym = reg[v[0]] = EnumSymbol(cls, k, *v)
                setattr(cls, k, sym)
        return type.__init__(cls, classname, bases, dict_)

    def __iter__(cls):
        return iter(cls._reg.values())


class DeclEnum(object):
    """Declarative enumeration."""

    __metaclass__ = EnumMeta
    _reg = {}

    @classmethod
    def from_string(cls, value):
        try:
            return cls._reg[value]
        except KeyError:
            raise ValueError("Invalid value for %r: %r" % (cls.__name__, value))

    @classmethod
    def values(cls):
        return cls._reg.keys()

    @classmethod
    def db_type(cls):
        return DeclEnumType(cls)


class DeclEnumType(SchemaType, TypeDecorator):
    def __init__(self, enum):
        self.enum = enum
        self.impl = Enum(*enum.values(),
                         name="ck%s" % re.sub('([A-Z])', lambda m: "_" + m.group(1).lower(), enum.__name__))

    def _set_table(self, table, column):
        self.impl._set_table(table, column)

    def copy(self):
        return DeclEnumType(self.enum)

    def process_bind_param(self, value, dialect):
        if value is None:
            return None
        return value.value

    def process_result_value(self, value, dialect):
        if value is None:
            return None
        return self.enum.from_string(value.strip())

# End Enum recipe }}}
