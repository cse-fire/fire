from sqlalchemy.schema import Column, ForeignKey, CheckConstraint
from sqlalchemy.types import Text, Integer, DateTime, Boolean
from sqlalchemy.orm import relationship

from datetime import datetime
from itertools import groupby

from slugify import slugify

from ..errors import InternalFireError

from .meta import Base, DBSession
from .submitters import Group, Student
from .graders import GraderWorkload
from .reviews import Review


class Lab(Base):
    __tablename__ = 'labs'

    id = Column(Integer, primary_key=True)
    title = Column(Text, nullable=False)
    slug = Column(Text, nullable=False)
    description = Column(Text)
    autoassign_grader = Column(Boolean, default=True, nullable=False)
    predecessor = Column(Integer, ForeignKey('labs.id', ondelete='CASCADE'))
    created = Column(DateTime, default=datetime.now, nullable=False)
    order = Column(Integer, default=0)
    active = Column(Boolean, default=True, nullable=False)
    individual = Column(Boolean, nullable=False)
    allow_physical_submissions = Column(Boolean, default=False, nullable=False)
    first_deadline = Column(DateTime)
    final_deadline = Column(DateTime)

    __mapper_args__ = {'polymorphic_on': individual,
                       'polymorphic_identity': True}

    # Bounds for individual labs, to be rewritten by GroupLab
    min_members = 1
    max_members = 1

    workload = relationship(GraderWorkload, cascade='all, delete-orphan')

    deadline_extensions = relationship(
        'IndividualDeadlineExtension',
        order_by='IndividualDeadlineExtension.date', cascade='all, delete-orphan')
    allowed_file_extensions = relationship(
        'AllowedFileExtension', cascade='all, delete-orphan')
    checks = relationship('CheckHook', cascade='all, delete-orphan')

    def __init__(self, title, **kwargs):
        super(Lab, self).__init__(title=title, **kwargs)
        if self.slug is None:
            self.slug = slugify(self.title)

    def grading_share(self, grader, relative=False):
        workload = next((a for a in self.workload if a.grader.id == grader.id), None)

        if not workload:
            return 0

        if relative:
            total_weight = sum(assignment.weight for assignment in self.workload)
            return float(workload.weight) / total_weight
        else:
            return workload.weight

    @property
    def responsible_graders(self):
        return [w.grader for w in self.workload if w.weight > 0]

    @property
    def is_suspended(self):
        return not self.responsible_graders

    @property
    def can_be_deleted(self):
        return self.submissions == []

    @classmethod
    def get_first_active_lab(cls, individual):
        """Returns the first lab of the given type."""
        return cls.query().filter_by(active=True, individual=individual).order_by(Lab.order).first()

    @classmethod
    def get_active(cls):
        return cls.query().filter_by(active=True).order_by(Lab.order).all()

    def get_group_size_exception(self, group):
        assert isinstance(group, Group)
        return GroupSizeException.get((self.id, group.id))

    def add_group_size_exception(self, group):
        assert isinstance(group, Group)
        exception = self.get_group_size_exception(group)
        if not exception:
            DBSession.add(GroupSizeException(self, group))

    def remove_group_size_exception(self, group):
        assert isinstance(group, Group)
        exception = self.get_group_size_exception(group)
        if exception:
            DBSession.delete(exception)

    def get_deadline_extension(self, submitter):
        if submitter is None:
            return None
        elif self.individual:
            if isinstance(submitter, Group):
                # Note that a group cannot have a deadline extension in an individual lab,
                # but any of the students in the group might, and this should be checked separately
                return None
            else:
                assert isinstance(submitter, Student)
                student = submitter
            return IndividualDeadlineExtension.get((self.id, student.email))
        else:
            if isinstance(submitter, Group):
                group = submitter
            else:
                assert isinstance(submitter, Student)
                if submitter.group is None:
                    # Student doesn't yet have a group
                    return None
                else:
                    group = submitter.group
            return GroupDeadlineExtension.get((self.id, group.id))

    def extend_deadline(self, submitter, first_deadline, final_deadline):
        extension = self.get_deadline_extension(submitter)
        if extension:
            extension.first_deadline = first_deadline
            extension.final_deadline = final_deadline
        else:
            if self.individual:
                assert isinstance(submitter, Student)
                student = submitter
                extension = IndividualDeadlineExtension(self, student, first_deadline, final_deadline)
            else:
                if isinstance(submitter, Group):
                    group = submitter
                else:
                    assert isinstance(submitter, Student)
                    assert submitter.group
                    group = submitter.group
                extension = GroupDeadlineExtension(self, group, first_deadline, final_deadline)
            DBSession.add(extension)

    def remove_deadline_extension(self, submitter):
        extension = self.get_deadline_extension(submitter)
        DBSession.delete(extension)

    def get_deadlines(self, submitter, full_info=False):
        extension = self.get_deadline_extension(submitter)

        first_extended = extension and extension.first_deadline
        final_extended = extension and extension.final_deadline

        first = extension.first_deadline if first_extended else self.first_deadline
        final = extension.final_deadline if final_extended else self.final_deadline

        if full_info:
            return dict(first=first, first_extended=first_extended,
                        final=final, final_extended=final_extended)
        else:
            return first, final

    def deciding_submission_for_submitter(self, submitter):
        """returns the submission that decided the status
           above, e.g. the latest accepted or rejected submission,
           or the latest non-cancelled submission if none are accepted
           or rejected.
        """
        if not submitter:
            return None

        submissions = submitter.submissions_for_lab(self)

        if submissions and all(not s.is_active for s in submissions):
            return submissions[-1]

        for submission in sorted(submissions, key=lambda s: s.date, reverse=True):
            if submission.is_active:
                return submission

        return None

    @property
    def active_submissions(self):
        return [sub for sub in self.submissions if sub.is_active]

    @property
    def pending_submissions(self):
        return sorted(filter(lambda s: s.is_pending, self.submissions), key=lambda s: s.date)

    def last_submissions(self, decision=None):
        # Group submissions by submitter
        ordered_submissions = sorted(self.active_submissions, key=lambda s: s.submitter)
        grouped_submissions = groupby(ordered_submissions, key=lambda s: s.submitter)

        last_submissions = []

        for submitter, submissions in grouped_submissions:
            latest_submission = max(submissions, key=lambda s: s.number)
            if decision is None or (latest_submission.reviews and latest_submission.reviews[-1].decision == decision):
                last_submissions.append(latest_submission)

        if decision:
            last_submissions.sort(key=lambda s: s.reviews[-1].date)

        return last_submissions

    @property
    def rejected_submissions(self):
        return self.last_submissions(Review.Decision.rejected)

    @property
    def accepted_submissions(self):
        return self.last_submissions(Review.Decision.accepted)

    def last_grader(self, submitter):
        previous_submissions = submitter.submissions_for_lab(self)
        for submission in sorted(previous_submissions, key=lambda s: s.date, reverse=True):
            if submission.assigned_grader:
                return submission.assigned_grader
        return None

    def choose_grader(self, submitter):
        # If this is not the first submission in the thread, reuse the grader
        # from the most recent previous submission
        previous_grader = self.last_grader(submitter)

        if previous_grader:
            return previous_grader

        # If the lab has a predecessor, reassign to its grader
        if self.predecessor:
            predecessor = Lab.get(self.predecessor)
            last_grader = predecessor.last_grader(submitter)
            if last_grader:
                return last_grader

        # Otherwise, assign a random grader
        if not self.workload:
            raise InternalFireError("Misconfiguration: No grader workload set on lab %d." % self.id)

        last_submissions = self.last_submissions()

        # Chosen grader is the one with lowest value for
        #   ("nr assigned submission" + 1) / "weight"
        # i.e., who would have the lowest current work load if that person
        # was assigned this submission.
        def work(assignment):
            grader = assignment.grader
            weight = float(assignment.weight)
            assigned = sum(submission.assigned_grader is grader for submission in last_submissions)
            return (assigned + 1) / weight

        return min(self.workload, key=work).grader

    def __repr__(self):
        return '<Lab: %s>' % self.title

    def __eq__(self, other):
        return isinstance(other, self.__class__) \
            and self.id == other.id \
            and self.title == other.title \
            and self.slug == other.slug \
            and self.description == other.description \
            and self.predecessor == other.predecessor \
            and self.created == other.created \
            and self.order == other.order \
            and self.active == other.active \
            and self.individual == other.individual \
            and self.first_deadline == other.first_deadline \
            and self.final_deadline == other.final_deadline \
            and self.allow_physical_submissions == other.allow_physical_submissions \
            and self.min_members == other.min_members \
            and self.max_members == other.max_members \
            and self.workload == other.workload \
            and self.deadline_extensions == other.deadline_extensions \
            and self.allowed_file_extensions == other.allowed_file_extensions


class GroupLab(Lab):
    __tablename__ = 'grouplabs'

    id = Column(Integer, ForeignKey('labs.id'), primary_key=True)
    max_members = Column(Integer,
                         CheckConstraint('max_members > 1'),
                         CheckConstraint('max_members >= min_members'),
                         default=2)
    min_members = Column(Integer, CheckConstraint('min_members > 0'), default=1)

    deadline_extensions = relationship('GroupDeadlineExtension', order_by='GroupDeadlineExtension.date')

    __mapper_args__ = {'polymorphic_identity': False}


class GroupDeadlineExtension(Base):
    __tablename__ = 'group_deadline_extensions'

    lab_id = Column(Integer, ForeignKey('labs.id'), nullable=False, primary_key=True)
    group_id = Column(Integer, ForeignKey('groups.id'), nullable=False, primary_key=True)

    date = Column(DateTime, nullable=False, default=datetime.now)  # the date of the last update

    first_deadline = Column(DateTime)
    final_deadline = Column(DateTime)

    lab = relationship(Lab)
    group = relationship(Group)

    @property
    def submitter(self):
        return self.group

    def __init__(self, lab, group, first_deadline, final_deadline):
        self.lab_id = lab.id
        self.group_id = group.id
        self.first_deadline = first_deadline
        self.final_deadline = final_deadline


class IndividualDeadlineExtension(Base):
    __tablename__ = 'individual_deadline_extensions'

    lab_id = Column(Integer, ForeignKey('labs.id'), nullable=False, primary_key=True)
    student_id = Column(Text, ForeignKey('students.email'), nullable=False, primary_key=True)

    date = Column(DateTime, nullable=False, default=datetime.now)  # the date of the last update

    first_deadline = Column(DateTime)
    final_deadline = Column(DateTime)

    lab = relationship(Lab)
    student = relationship(Student)

    @property
    def submitter(self):
        return self.student

    def __init__(self, lab, student, first_deadline, final_deadline):
        self.lab_id = lab.id
        self.student_id = student.id
        self.first_deadline = first_deadline
        self.final_deadline = final_deadline


class AllowedFileExtension(Base):
    """ This class represent an allowed file extension for a lab.  For instance
    a database lab may only allow .sql and .pdf files to be submitted.
    It is implemented as a multi-valued attribute to Lab.
    """
    __tablename__ = 'allowed_file_extensions'

    lab_id = Column(Integer, ForeignKey('labs.id'), primary_key=True)
    extension = Column(Text, primary_key=True)

    def __init__(self, extension, **kw):
        super(self.__class__, self).__init__(extension=extension, **kw)
        assert extension.startswith('.'), \
            "A file extension must start with '.'"

    def __eq__(self, other):
        return isinstance(other, self.__class__) \
            and self.lab_id == other.lab_id \
            and self.extension == other.extension

    def __repr__(self):
        return "<AllowedFileExtension {}".format(self.extension)

    def __str__(self):
        return self.extension


class GroupSizeException(Base):
    __tablename__ = 'group_size_exceptions'

    lab_id = Column(Integer, ForeignKey('labs.id'), nullable=False, primary_key=True)
    group_id = Column(Integer, ForeignKey('groups.id'), nullable=False, primary_key=True)

    date = Column(DateTime, nullable=False, default=datetime.now)

    lab = relationship(Lab)
    group = relationship(Group)

    def __init__(self, lab, group):
        self.lab_id = lab.id
        self.group_id = group.id
