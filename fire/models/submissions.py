from sqlalchemy.schema import (
    Table, Column, ForeignKey, CheckConstraint, UniqueConstraint)
from sqlalchemy.types import Integer, Text, DateTime, Boolean
from sqlalchemy.orm import relationship

from datetime import datetime

from .. import filestore

from .meta import Base, DBSession
from .submitters import Group, Student
from .graders import Grader
from .reviews import Review
from .instrumental import MarkdownType


submitters_table = Table('submitters', Base.metadata,
                         Column('submission_id', Integer, ForeignKey('submissions.id')),
                         Column('user_id', Text, ForeignKey('users.email')))


class Submission(Base):
    __tablename__ = 'submissions'
    __table_args__ = tuple(UniqueConstraint('lab_id', 'group_id', 'number'))

    id = Column(Integer, primary_key=True)
    lab_id = Column(Integer, ForeignKey('labs.id'), nullable=False)
    group_id = Column(Integer, ForeignKey('groups.id'))
    number = Column(Integer, nullable=False)
    comment = Column(MarkdownType)

    date = Column(DateTime, default=datetime.now, nullable=False)

    physical_submission = Column(Boolean, nullable=False)
    physical_submission_received_by_id = Column(Text, ForeignKey('graders.email'))
    physical_submission_received_date = Column(DateTime)

    lab = relationship('Lab', backref='submissions')
    group = relationship(Group, backref='submissions', lazy=False)
    physical_submission_received_by = relationship(Grader, primaryjoin=physical_submission_received_by_id == Grader.email)
    submitters = relationship(Student, secondary=submitters_table, backref='submissions')

    grader_assignment = relationship(
        'GraderAssignment', uselist=False, lazy='joined',
        cascade='all, delete-orphan')
    reviews = relationship(Review, lazy='joined', order_by="Review.date")
    withdrawal = relationship('WithdrawnSubmission', uselist=False, lazy='joined')

    def __init__(self, lab, submitter, comment, physical_submission):
        assert isinstance(submitter, Student if lab.individual else Group)
        super(Submission, self).__init__(
            lab=lab,
            number=len(submitter.submissions_for_lab(lab)) + 1,
            group=submitter if isinstance(submitter, Group) else None,
            submitters=submitter.valid_members(lab),
            physical_submission=lab.allow_physical_submissions and physical_submission,
            comment=comment
        )

    @property
    def submitter(self):
        return self.group or self.submitters[0]

    @property
    def files(self):
        return filestore.files_for_submission(self)

    @property
    def path(self):
        return filestore.path_for_submission(self)

    @property
    def is_active(self):
        return not self.withdrawal

    @property
    def is_pending(self):
        return self.is_active and not self.reviews

    @property
    def assigned_grader(self):
        return self.grader_assignment.grader if self.is_active and self.grader_assignment else None

    @property
    def review(self):
        """ This represents the latest review and it is named simply review
        for backward compatibility (before there could be only one review per
        submission).  It is also the review that gives the overall status
        (accept/reject) the whole submission. """
        return self.reviews[-1] if self.reviews else None

    def can_be_withdrawn(self):
        # Previously withdrawn submissions cannot be withdrawn again
        if not self.is_active:
            return False

        # Reviewed submissions cannot be withdrawn
        if self.reviews:
            return False

        """A submission cannot be withdrawn after its effective deadline
        Note that in particular it means the following:
         * a submission can be withdrawn before the first deadline if none of the previous submissions
           were reviewed (that is, were withdrawn);
         * a submission can be withdrawn after the first deadline and before the final deadline if there were
           submissions that were reviewed (that is, rejected);
         * a submission cannot be withdrawn after the final deadline.
        """
        if not self.submitter.in_time(self.lab):
            return False

        return True

    def assign_grader(self, grader):
        self.grader_assignment = GraderAssignment(self, grader)

    def withdraw(self):
        assert self.can_be_withdrawn()
        self.withdrawal = WithdrawnSubmission(self)

    @property
    def reviewable(self):
        return self.is_active

    def post_review(self, accepted, grader, grade, note, review):
        assert self.reviewable
        self.reviews.append(Review(
            self, accepted, grader.id, note=note, grade=grade, review=review))


class GraderAssignment(Base):
    __tablename__ = 'grader_assignments'
    submission_id = Column(Integer, ForeignKey('submissions.id'), primary_key=True)
    grader_id = Column(Text, ForeignKey('graders.email'), nullable=False)

    date = Column(DateTime, default=datetime.now, nullable=False)

    grader = relationship(Grader, uselist=False, lazy='joined', backref='assignments')
    submission = relationship(Submission, uselist=False, lazy='joined')

    def __init__(self, submission, grader):
        self.submission_id = submission.id
        self.grader_id = grader.id


class WithdrawnSubmission(Base):
    __tablename__ = 'withdrawn_submissions'

    submission_id = Column(Integer, ForeignKey('submissions.id'), primary_key=True)
    date = Column(DateTime, default=datetime.now, nullable=False)

    def __init__(self, submission):
        self.submission_id = submission.id
