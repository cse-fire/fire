""" Models for automated checks.

Automated checks are like continuous integration jobs (e.g. travis).  There are
two parts in storing the checks in fire: first graders add check hooks which
are urls that will be requested when a new submission created.  A hook is
connected to a particular lab.  The other part, is storing the check result,
which is connected to a particular submission.

There is intentionally no foreign key between the check result and the check
hook so that we can keep the history of check results even if a check hook
is deleted.
"""
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy.orm import relationship
from sqlalchemy.schema import Column, ForeignKey
from sqlalchemy.types import Boolean, Text, Integer, Enum

from string import Template
from urllib import quote

from ..security import random_hex_string

from .meta import Base
from .instrumental import MarkdownType
from .submissions import Submission


class CheckHook(Base):
    """ This holds the URL to an automatic check job on a CI server """
    __tablename__ = 'check_hooks'

    name = Column(Text, primary_key=True)
    lab_id = Column(Integer, ForeignKey('labs.id'), primary_key=True)
    url_template = Column(Text, nullable=False)
    auto_accept = Column(Boolean, nullable=False)
    auto_reject = Column(Boolean, nullable=False)

    results = relationship('CheckResult', cascade='all, delete-orphan')

    def __repr__(self):
        return "<CheckHook {} (lab {})>".format(self.name, self.lab_id)

    def get_url(self, submission_url, feedback_url):
        mapping = dict(
            submission_url=quote(submission_url),
            feedback_url=quote(feedback_url))
        return Template(self.url_template).safe_substitute(mapping)


class CheckResult(Base):
    __tablename__ = 'check_results'

    PENDING = 'PENDING'
    PASS = 'PASS'
    FAIL = 'FAIL'

    token = Column(Text, primary_key=True)
    submission_id = Column(Integer, ForeignKey(Submission.id), nullable=False)

    check_name = Column(Text, nullable=False)
    check_lab_id = Column(Integer, nullable=False)
    __table_args__ = (ForeignKeyConstraint((check_name, check_lab_id),
                                           (CheckHook.name, CheckHook.lab_id)), {})

    status = Column(Enum(PENDING, PASS, FAIL), nullable=False, default=PENDING)
    details_url = Column(MarkdownType, nullable=True)

    submission = relationship(Submission, backref='checks')
    check = relationship(CheckHook)

    def __init__(self, **kwargs):
        super(CheckResult, self).__init__(**kwargs)
        if self.token is None:
            self.token = random_hex_string(32)
