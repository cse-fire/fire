import mistune
from mistune import Renderer, BlockLexer, InlineLexer, escape
import asciimathml
from xml.etree.ElementTree import tostring

# This code is based on
# https://github.com/lepture/mistune-contrib/blob/master/mistune_contrib/math.py

import re
import settings
from genshi.core import Markup

class MarkdownBlockLexer(BlockLexer):
    def __init__(self, *args, **kwargs):
        BlockLexer.__init__(self, *args, **kwargs)
        self.enable_math()

    def enable_math(self):
        self.rules.block_math = re.compile(r'^\$\$(.*?)\$\$', re.DOTALL)

        self.default_rules.insert(0, 'block_math')

    def parse_block_math(self, m):
        """Parse a $$math$$ block"""
        self.tokens.append({
            'type': 'block_math',
            'text': m.group(1)
        })


class MarkdownInlineLexer(InlineLexer):
    def __init__(self, *args, **kwargs):
        InlineLexer.__init__(self, *args, **kwargs)
        self.enable_math()

    def enable_math(self):
        self.rules.text = re.compile(r'^[\s\S]+?(?=[\\<!\[_*`~\$]|https?://| {2,}\n|$)')
        self.rules.math = re.compile(r'^\$(?![\s])(("[^"\n]*"|[^\$\n])+?)(?<![\s])\$')
        self.default_rules.insert(0, 'math')

    def output_math(self, m):
        return self.renderer.math(m.group(1))

    
class MarkdownRenderer(Renderer):
    def __init__(self, *args, **kwargs):
        Renderer.__init__(self, *args, **kwargs)

    def __escape_script(self, text):
        return text.replace("</","< /")
    
    """We disable the image renderer to avoid privacy issues."""
    def image(self, src, title, alt_text):
        return self.link(src, title, alt_text)

    def block_math(self, text):
        tree = asciimathml.parse(text)
        tree.set('display', 'block')
        return tostring(tree)

    def math(self, text):
        tree = asciimathml.parse(text)
        tree.set('display', 'inline')
        return tostring(tree)


class MarkdownExtra(mistune.Markdown):
    def __init__(self):
        renderer = MarkdownRenderer(escape = True)

        mistune.Markdown.__init__(self,
            renderer = renderer,
            inline = MarkdownInlineLexer,
            block = MarkdownBlockLexer)

    def output_block_math(self):
        return self.renderer.block_math(self.token['text'])


class MarkdownView(object):
    "Views markdown objects according to configuration"
    def __init__(self, config):
        self.is_rich = settings.get(config, 'markdown')
        if self.is_rich: 
            self.Wrapper = MarkdownRichHTML
        else:
            self.Wrapper = MarkdownPlainHTML

    def __call__(self, md):
        return self.Wrapper(md)

class MarkdownRichHTML(object):
    """Rich representation of markdown code, respecting spacing, with formatting."""
    render_markdown = MarkdownExtra()

    def __init__(self, md):
        self.md = md

    def as_html(self):
        return Markup(MarkdownRichHTML.render_markdown(self.md.source()))


class MarkdownPlainHTML(object):
    """Plain-text representation of markdown code, respecting spacing
    but without formatting."""
    def __init__(self, md):
        self.md = md

    def as_html(self):
        return Markup('<p class="verbatim">%s</p>' % (mistune.escape(self.md.source()),))
