import sys
import os
import textwrap

from pyramid.paster import setup_logging, bootstrap, get_appsettings
from pyramid.request import Request

from .. import settings as fire_settings

doc = """Fire admin tool (Fire v.%(version)s)

Usage:
%(commands)s

Options:
  -h --help               Show this screen.
  --version               Show the version of Fire installed.
  -c FILE --config=FILE   Read configuration from file. [default: ~/fire.conf]
"""

# globals initialized in fireadmin()
_commands = None
_cmd_name_map = None


def help(settings, arguments):
    """help <command>
    Prints detailed help for <command>
    """
    cmd = _cmd_name_map[arguments['<command>']]
    print("Usage:\n  fireadmin " + cmd.__doc__)


def fireadmin():
    import sys
    from docopt import docopt
    import pkg_resources

    from . import sampledata, settings, init, adduser

    global _commands, _cmd_name_map

    _commands = [
        settings.main,
        init.main,
        sampledata.main,
        adduser.main,
        help,
        ]

    _cmd_name_map = {cmd.__doc__.split()[0]: cmd for cmd in _commands}

    cmds_doc = '\n'.join("  fireadmin [options] " + cmd.__doc__.split('\n')[0] for cmd in _commands)
    version = pkg_resources.require('fire')[0].version
    arguments = docopt(doc % {'version': version,
                              'commands': cmds_doc}, version=version)

    # Initialize app
    config_uri = os.path.expanduser(arguments['--config'])
    base_url = get_appsettings(config_uri).get('fire.url_host')
    request = Request.blank('/', base_url=base_url)
    env = bootstrap(config_uri, request=request)
    setup_logging(config_uri)

    # Find the selected command
    for name, cmd in _cmd_name_map.items():
        if arguments[name]:
            sys.exit(cmd(env['request'], arguments))
            break  # in case cmd throws exception before sys.exit runs


def confirm_dangerous(msg, answer='yes'):
    tw = textwrap.wrap(textwrap.dedent(msg), 48)
    print ""
    print "   " + 56*"#"
    print "   ##############  WARNING! DANGER AHEAD!  ################"
    print "   " + 56*"#"
    for line in tw:
        print "   ### %-48s ###" % line
    print "   " + 56*"#"
    print ""
    inp = raw_input("Type 'confirm' (without quotes) to confirm: ")
    if inp.strip() == 'confirm':
        print "Confirmed, going ahead."
    else:
        print "Cancelled, exiting without any further action."
        sys.exit()
