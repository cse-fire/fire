import transaction
from datetime import datetime, timedelta
import random
import re

from sqlalchemy import engine_from_config

from . import confirm_dangerous

from ..models import (
    CourseCode, Student, Grader, Settings, DBSession, GroupLab, GraderWorkload,
    Group)


def main(_, arguments):
    """sampledata [--non-interactive]

    Populates the database with sample data for debugging.
    """

    if not arguments['--non-interactive']:
        confirm_dangerous("""
        This command will insert sample data into the database configured
        to aid development.
        DO NOT use this command for live systems.
        """)

    session = DBSession()

    with transaction.manager:
        create_sample_data(session)


def create_sample_data(session):
    ccodes = ['TDA602', 'TDA601', 'DIT103', 'DIT101']
    for code in ccodes:
        session.add(CourseCode(code=code))

    Settings.set('course_name', 'Language-Based Security')

    graders = [('arnar.birgisson@chalmers.se', 'Arnar Birgisson', 'changeme'),
               ('andrei@chalmers.se', 'Andrei Sabelfeld', 'changeme'),
               ('jonas.magazinius@chalmers.se', 'Jonas Magazinius', 'changeme')]

    for email, name, pwd in graders:
        grader = Grader(email)
        grader.first_name, grader.last_name = name.split()
        grader.password = pwd
        session.add(grader)

    def create_student(fname, lname, group=None):
        """ Create a student with the given first and last names and add them to
        the given group if any (creats the group as well if necessary """
        email = (
            fname[0] + re.sub(r'[\W_]', '', lname) + "@stu.springfield.edu"
        ).lower()
        student = Student(email, first_name=fname, last_name=lname,
                          password='changeme', course=random.choice(ccodes),
                          id_number=str(random.randrange(9999999999)))
        session.add(student)
        if group is not None:
            student_group = session.query(Group).get(group) or Group(id=group)
            student_group.members.append(student)
            session.add(student_group)

    create_student("Martin", "Prince", group=1)
    create_student("Bart", "Simpson", group=2)
    create_student("Milhouse", "Van Houten", group=2)

    u1 = Student('arnarbi@gmail.com')
    u1.password = 'test'
    u1.first_name = 'Student'
    u1.last_name = 'Birgisson'
    u1.course = 'TDA602'
    u1.id_number = '0000000000'
    session.add(u1)

    u2 = Student('arnar@hvergi.net')
    u2.password = 'test'
    u2.first_name = 'Arnar'
    u2.last_name = 'Studentsson'
    u2.course = 'TDA602'
    u2.id_number = '0000000001'
    session.add(u2)

    week = timedelta(days=7)
    now = datetime.now()

    labs = [(1, 'Project proposal', 'proposal', now - 3 * week, 'andrei@chalmers.se'),
            (2, 'Eraser', 'eraser', now - 1 * week, 'arnar.birgisson@chalmers.se'),
            (3, 'VM Account', 'account', now + week, 'arnar.birgisson@chalmers.se'),
            (4, 'r00tshell', 'r00tshell', now + 2 * week, 'arnar.birgisson@chalmers.se'),
            (5, 'WebAppSec', 'webappsec', now + 3 * week, 'arnar.birgisson@chalmers.se'),
            (6, 'Project report', 'project', now + 4 * week, 'andrei@chalmers.se')]

    for order, title, slug, deadline, grader in labs:
        l = GroupLab(title=title, slug=slug,
                     first_deadline=deadline,
                     final_deadline=deadline + 2 * week,
                     order=order)
        for g in Grader.get_all():
            l.workload.append(GraderWorkload(grader=g, lab=l))
        l.max_members = 2
        l.min_members = 2
        session.add(l)
