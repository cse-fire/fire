import transaction

from sqlalchemy import engine_from_config

from ..models import DBSession, Settings


def main(_, arguments):
    """settings [<setting-name> [<new-value>]]

    Sets or retrieves course-specific settings.
    Lists all settings if no setting-name is specified
    """
    setting_name = arguments['<setting-name>']
    new_value = arguments['<new-value>']

    with transaction.manager:
        if new_value:
            Settings.set(setting_name, new_value)

        print Settings.get(setting_name)
