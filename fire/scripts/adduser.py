from ..models import EmailVerification, Settings, User
from ..mail import sendmail
import transaction
import sys


def main(request, arguments):
    """adduser <email-address>

    Send a verification email to the specified address
    """

    email = arguments['<email-address>']

    with transaction.manager:
      if User.get(email) is not None:
        print('[Error] Email %s already exists' % email)
        sys.exit(1)

      verification = EmailVerification.create(email=email)

      signup_url = request.route_url('signup', _query={'token': verification.token})
      sendmail(request, [email],
              'Please verify your Fire account',
              'fire:templates/mails/verify_email.txt',
              dict(url=signup_url, course_name=Settings.get('course_name')))

    print('A verification link has been sent to %s.' % email)
