from __future__ import unicode_literals

from email.utils import parseaddr
import logging
import os
import sys

import transaction

from ..mail import sendmail
from ..models import DBSession, Base, Admin, Settings, CourseCode
from ..security import pwgen


LOG = logging.getLogger(__name__)


def main(request, arguments):
    """init [--name=<name>] [--url=<url>] [--admin=<admin>] [--codes=<code>]

    Initializes a fire instance.  This command will do the following:

    * create the data directory if it doesn't exist
    * initialize the database
    * set the course name (if specified)
    * set the course code(s) (if specified)
    * create a course administrator (if specified) and send them a welcome
      email with their password and a link to the fire instance.

    This command is idempotent and won't override modified data so it is safe
    to call it multiple times.

    Parameters:
      --name=<name>   Course name
      --url=<url>   Course URL
      --admin=<admin> Course admin, in the form 'John Doe <john@example.com>'
      --codes=<codes> Comma separated list of course codes
    """
    data_dir = request.registry.settings['fire.data_dir']
    if not os.path.exists(data_dir):
        LOG.info("Creating directory %s", data_dir)
        os.makedirs(data_dir)
    elif not os.path.isdir(data_dir):
        LOG.error("%s exists but is not a directory, aborting.", data_dir)
        sys.exit(1)

    LOG.info("Initializing database")
    Base.metadata.create_all(DBSession.get_bind())

    with transaction.manager:
        if arguments['--name']:
            set_course_name(arguments['--name'])

        if arguments['--url']:
            set_course_url(arguments['--url'])

        if arguments['--codes']:
            set_course_codes(arguments['--codes'].split(','))

        if arguments['--admin']:
            add_admins(request, arguments['--admin'])


def set_course_name(name):
    """ This function sets the course name, if different from the current
    value.  It means that it will override the existing name, which is
    the desired behavior for now.  If at some point there is an interface
    allowing course administrators to change the course name themselves,
    then we might want to change this behavior. """
    current_name = Settings.get('course_name')
    if name != current_name:
        LOG.info("Setting course name: %s", name)
        Settings.set('course_name', name)
    else:
        LOG.info("Course name not changed")


def set_course_url(url):
    current_url = Settings.get('course_url')
    if url != current_url:
        LOG.info("Setting course URL: %s", url)
        Settings.set('course_url', url)
    else:
        LOG.info("Course URL not changed")


def set_course_codes(codes):
    """ Set the course codes.  Given codes that aren't present in the database
    are added but existing code are not removed because those codes may
    already be used by some students """
    for code in codes:
        if DBSession.query(CourseCode).get(code) is None:
            LOG.info("Adding course code: %s", code)
            DBSession.add(CourseCode(code=code))
        else:
            LOG.info("Course code already exists: %s", code)


def add_admins(request, admins):
    """ Add a course admin.  Currently, fire doesn't distinguish between
    graders and course admin, but the idea is that this person will be
    responsible to add labs and other graders.
    Note that in this case, we only check that there is no grader with the
    same email already in the database but we don't overwrite their name if
    different as the grader may have changed their name themselves.
    """
    admins = admins.decode('utf-8')  # because docopt returns str values
    for admin in admins.split(";"):
        add_admin(request, admin)


def add_admin(request, admin):
    """ Add a course admin.  Currently, fire doesn't distinguish between
    graders and course admin, but the idea is that this person will be
    responsible to add labs and other graders.
    Note that in this case, we only check that there is no grader with the
    same email already in the database but we don't overwrite their name if
    different as the grader may have changed their name themselves.
    """
    name, email = parseaddr(admin)
    if DBSession.query(Admin).get(email) is not None:
        LOG.info("%s is already in the database, skipping", email)
        return

    LOG.info("Adding administrator: %s", admin)

    names = name.split(' ', 1)
    first_name = names.pop(0)
    last_name = names.pop(0)

    password = pwgen()
    DBSession.add(Admin(email, first_name=first_name, last_name=last_name, password=password))

    fireurl = request.route_url('login')
    subject = "Your new Fire system"

    sendmail(
        request, [email], subject, 'fire:templates/mails/admin_welcome.txt',
        dict(fireurl=fireurl, name=name, email=email, password=password,
             course=Settings.get('course_name'))
    )
