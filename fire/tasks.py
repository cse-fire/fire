""" This module defines "background" tasks that are executed by the fire system
but not necesseraly connected directly with a user action.

There are two types of tasks: time-based or event based.  Time-based tasks are
run with a specific interval or at a specific time (think cron jobs).  Event
based task are triggered by an event in an other part of the system (e.g. when
a new submission is created).

"""
import threading
from datetime import datetime, timedelta
from itertools import groupby
from operator import attrgetter
import logging
import urllib2

from apscheduler.schedulers.background import BackgroundScheduler
from pyramid.threadlocal import get_current_request
from pyramid.events import subscriber
from pyramid.request import Request
import transaction

from fire.events import NewSubmission
from . import mail
from .filestore import files_for_submission
from .models import (
    DBSession, PasswordReset, FileCap, CheckResult, Grader, Settings)
from .security import random_hex_string
import fire.settings as settings


LOG = logging.getLogger(__name__)


def includeme(config):
    """ Set-up the scheduler for the pyramid app (one can think of it as an
    internal cron).  Add time-based jobs """
    config.scheduler = BackgroundScheduler()
    config.scheduler.start()
    config.add_request_method(lambda _: config.scheduler, 'scheduler',reify=True,property=True)

    # Every 10 minutes, remove expired password-resets
    config.scheduler.add_job(clean_password_resets, 'interval', minutes=10)
    config.scheduler.add_job(send_notifications, 'cron', args=[config], hour=6)


# ~~~ GRADER NOTIFICATIONS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def send_notifications(config):
    """ Send notification to all graders with the list of submissions assigned
    to them dring the last 24 hours """
    config.begin(Request.blank('', base_url=config.get_settings()['fire.url_host']))
    with transaction.manager:
        request = get_current_request()
        session = DBSession()
        from_date = datetime.now() - timedelta(hours=24)
        for grader in session.query(Grader).all():
            new_submissions = [
                a.submission for a in grader.assignments
                if a.date >= from_date
                and a.submission.is_active
                and a.submission.review is None
            ]
            if not new_submissions:
                LOG.debug("No new submissions for %s", grader)
            else:
                LOG.debug("%d new submissions for %s",
                          len(new_submissions), grader)
                course_name = Settings.get('course_name')
                subject = "New submissions in %s" % course_name
                submissions = groupby(
                    sorted(new_submissions, key=attrgetter('lab')),
                    key=attrgetter('lab'))
                mail.sendmail(config, [grader.email], subject,
                              'fire:templates/mails/notify_submissions.txt',
                              dict(url=request.route_url,
                                   submissions=submissions,
                                   grader=grader,
                                   course_name=course_name))
    config.end()


# ~~~ DATABASE CLEANING ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def clean_password_resets():
    """ Delete password-reset links created more than 48 hours ago """
    with transaction.manager:
        max_cdate = datetime.now() - timedelta(hours=48)
        DBSession().query(PasswordReset).\
            filter(PasswordReset.cdate < max_cdate).delete()


# ~~~ AUTOMATED CHECKS TRIGGERING ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def filecap_url(request, filecap):
    cap_path = request.route_path(
        'get_filecap', filecap_id=filecap.id, filename=filecap.filename)
    return settings.get(request, 'url_capserver') + cap_path


@subscriber(NewSubmission)
def on_new_submission(event):
    LOG.debug("Triggering automadet checks for submission %s", event.submission)
    files = files_for_submission(event.submission)
    for check in event.submission.lab.checks:
        job = CheckResult(submission=event.submission, check=check)
        job.token = random_hex_string(32)
        cap = FileCap.issue_archive(
            [(f.path, f.name) for f in files['files']], 'submission.tgz', 3600)
        submission_url = filecap_url(event.request, cap)
        feedback_url = event.request.route_url('check_feedback', token=job.token)
        job.url = check.get_url(submission_url, feedback_url)
        LOG.debug("Running automated check '%s' on URL '%s'" % (check.name, job.url))
        event.submission.checks.append(job)
        # event.request.scheduler.add_job(urllib2.urlopen, args=[job.url])
        threading.Thread(target=urllib2.urlopen, args=(job.url,), kwargs={}).start()
