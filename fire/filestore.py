import os
import errno
import uuid
from datetime import datetime
import unicodedata
import shutil
import tempfile

from . import settings
from . import errors

"""File storage and processing."""

import logging
log = logging.getLogger(__name__)


class DraftDir(object):
    """
    For a given lab and submitter (student or group depending on the lab type),
    a unique directory where un-submitted files are kept.
    """

    def __init__(self, datadir, lab, submitter):
        self.path = os.path.abspath(os.path.normpath(os.path.join(
            datadir, 'drafts', 'lab%02d' % lab.id, submitter.slug)))

    def store_file(self, file_name, file_data):
        try:
            fd, tmp_path = tempfile.mkstemp()
            file_data.seek(0)
            while True:
                data = file_data.read(2 << 16)
                if not data:
                    break
                os.write(fd, data)
            os.close(fd)

            ensure_dir_exists(self.path)
            fn = sanitize_filename(file_name)
            final_path = os.path.join(self.path, fn)

            shutil.move(tmp_path, final_path)
            return FileMetadata(final_path)
        except errors.UserException, e:
            raise e
        except Exception, e:
            log.exception('')
            raise errors.InternalFireError(e)
            pass

    def delete_file(self, file_path):
        delete_file(self.path, file_path)

    def ls(self):
        return files_for_path(self.path)


def ensure_dir_exists(path):
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


def sanitize_filename(fn):
    # IE sends absolute paths as file names, so in case there are
    # backslashes, we grab only the last part.
    if '\\' in fn:
        fn = fn.split('\\')[-1]
        assert len(fn) > 0

    fn = fn.replace('\\', '-')   # redundant because of above, but just in case
    fn = fn.replace('/', '-')
    fn = fn.replace(':', '-')
    fn = fn.replace('~', '-')

    return fn


def path_for_submission(submission):
    lab_str = "lab%02d" % submission.lab.id
    submitter_str = submission.submitter.slug
    submission_str = "submission%02d" % submission.number

    path = os.path.join(settings.get_static('data_dir'), 'filestore', lab_str, submitter_str, submission_str)
    return os.path.abspath(os.path.normpath(path))


text_extensions = ['', '.txt', '.pml', '.md']
image_extensions = ['.jpg', '.jpeg', '.gif', '.png', '.pdf']
web_extensions = ['.js', '.html']
source_extensions = ['.java', '.hs', '.lhs', '.py', '.cpp', '.csharp', '.sql',
                     '.agda', '.lagda', '.tex', '.erl', '.sh', '.scala', '.xml',
                     '.lisp', '.cl', '.clj', '.scm', '.pl', '.rb', '.cf']
browsable_extensions = text_extensions + image_extensions + web_extensions + source_extensions


class FileMetadata(object):
    def __init__(self, file_path):
        self._name = os.path.basename(file_path)
        self._path = file_path
        self._size = os.path.getsize(file_path)
        self._utime = datetime.fromtimestamp(os.path.getmtime(file_path))

        extension = os.path.splitext(file_path)[1].lower()
        # For performance reasons we only check for file extension here.
        # More thorough check is performed immediately before browsing.
        self._browsable = extension in browsable_extensions
        self._sensitive = extension in web_extensions

    @property
    def name(self):
        return self._name

    @property
    def size(self):
        return self._size

    @property
    def path(self):
        return self._path

    @property
    def utime(self):
        return self._utime

    @property
    def browsable(self):
        return self._browsable

    @property
    def sensitive(self):
        return self._sensitive


def delete_file(path, file_path):
    full_path = os.path.join(path, file_path)

    if not os.path.exists(full_path):
        log.error("attempted delete of non-existing file %s", full_path)
        raise errors.InternalFireError("delete_file: File does not exist")

    ensure_dir_exists(os.path.join(path, '.deleted'))

    now = datetime.now()

    del_path = os.path.join('.deleted', now.strftime('%Y%m%d%H%M%S-') + file_path)

    os.rename(full_path, os.path.join(path, del_path))


def delete_submission_file(submission, file_path):
    delete_file(path_for_submission(submission), file_path)


fire_comment_file = 'FIRE_COMMENT.txt'
fire_info_file = 'FIRE_INFO.txt'
fire_reserved_files = set([fire_comment_file, fire_info_file])


def store_comment(submission, comment):
    """
    submission : Submission
    comment : Markdown
    """
    path = os.path.join(path_for_submission(submission), fire_comment_file)
    with open(path, 'wb') as comment_file:
        comment_file.write(comment.source().encode('utf8'))


def store_info(submission, info):
    """
    submission: Submission
    info: string
    """
    path = os.path.join(path_for_submission(submission), fire_info_file)
    with open(path, 'wb') as info_file:
        info_file.write(info.encode('utf8'))


def files_for_path(path, reserved = False):
    """Returns a dict with two lists, files and deleted_files.
    The first list contains FileMetadata objects, the second dicts with the
    following keys:
     - name : The base name of the file
     - path : The path to the actual file, relative to the submission dir
     - size : The size of the file in bytes, according to the filesystem

    Keyword arguments:
    reserved -- Include reserved files, such as comment and reviews.
    """
    if not os.path.isdir(path):
        return dict(files=[], deleted_files=[])

    def file_info(file_path):
        file_path = unicode(file_path)
        basename = os.path.basename(file_path)
        return dict(name=basename,
                    path=basename,
                    size=os.path.getsize(file_path),
                    utime=datetime.fromtimestamp(os.path.getmtime(file_path)))

    # IMPORTANT
    # os.listdir returns the Unicode versions of files
    # if the path is itself unicode
    # (see https://docs.python.org/2/howto/unicode.html#unicode-filenames)
    files = [FileMetadata(os.path.join(path, p))
             for p in os.listdir(unicode(path))
             if not p.startswith('.') and (reserved or p not in fire_reserved_files)]

    files.sort(key=lambda i: i.name)

    deleted_dir = os.path.join(path, '.deleted')
    if os.path.isdir(deleted_dir):
        # IMPORTANT
        # os.listdir returns the Unicode versions of files
        # if the path is itself unicode
        # (see https://docs.python.org/2/howto/unicode.html#unicode-filenames)
        deleted_files = [file_info(os.path.join(deleted_dir, p))
                         for p in os.listdir(unicode(deleted_dir))
                         if not p.startswith('.')]
        for info in deleted_files:
            dt, fn = info['name'].split('-', 1)
            info['path'] = os.path.join('.deleted', info['name'])
            info['deleted'] = datetime.strptime(dt, '%Y%m%d%H%M%S')
            info['name'] = fn
        deleted_files.sort(key=lambda i: (i['name'], i['deleted']))
    else:
        deleted_files = []

    return dict(files=files, deleted_files=deleted_files)


def files_for_submission(submission, reserved = False):
    return files_for_path(path_for_submission(submission), reserved = reserved)


def materialise(draftdir, submission):
    assert isinstance(draftdir, DraftDir)
    draft_path = draftdir.path
    submission_path = path_for_submission(submission)
    if os.path.exists(draft_path):
        os.renames(draft_path, submission_path)
    else:
        os.makedirs(submission_path)


def fullpath_for_file(dir, file_path):
    path = os.path.join(dir, file_path)
    return path if os.path.isfile(path) else None


def fullpath_for_submission_file(submission, file_path):
    return fullpath_for_file(path_for_submission(submission), file_path)
